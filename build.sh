# /usr/bin/bash

rm -rf build && rm -rf bin && mkdir build && cd build && cmake -DPOKY_DIR=$1 -DCMAKE_BUILD_TYPE=$2 ../src && make -j 8

make install

