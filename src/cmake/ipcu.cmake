
set(IPCU_DIRS ${CMAKE_SOURCE_DIR}/../src/ipcu)

unset (IPCU_INCLUDE_DIR CACHE)
unset (IPCU_LIBRARY_DIRS CACHE)

set(IPCU_LIBRARIES cmfwk)
include_directories(${IPCU_DIRS}/include )
link_directories( ${IPCU_DIRS}/ipcu_lib/libcmfwk )

message(STATUS "*** IPCU Include Dirs: ${IPCU_DIRS}")
message(STATUS "*** IPCU Library Dirs: ${IPCU_DIRS}")
message(STATUS "*** IPCU Libraries: ${IPCU_LIBRARIES}")

  