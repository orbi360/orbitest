#include "check_last.h"
#include <regex>

bool enum_dir(std::string const &dirname, std::function<bool(const char *)> callback)
{
    struct dirent *dp;
    DIR *dfd = opendir(dirname.c_str());
    if (!dfd)
    {
        // "enum dir: cant open dir (" << dirname << ")");
        return false;
    }

    while ((dp = readdir(dfd)) != NULL)
    {
        if (!callback(dp->d_name))
            break;
    }

    closedir(dfd);

    return true;
}

uint32_t get_file_size(std::string const &filename)
{
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : 0;
}

void CheckLast::check_media()
{
    bool video = false;

    std::uint32_t max_record_id = 0;
    std::regex dir_match((video ? ORBI_VIDEO_DIR_REGEXP : ORBI_IMAGE_DIR_REGEXP));
    std::smatch results;

    if (!enum_dir(ORBI_MEDIA_DIR, [&](const char *name) -> bool {
            const std::string name1 = name;
            if (std::regex_search(name1, results, dir_match, std::regex_constants::match_default))
            {
                std::string record_id_str = std::string(results[1].first, results[1].second);
                //std::cout <<  "IPCUWorker: dir found " << name1 << ", record id=" << record_id_str << "\n";

                try
                {
                    std::uint32_t record_id = std::stoll(record_id_str);
                    if (record_id > max_record_id)
                        max_record_id = record_id;
                }
                catch (std::exception const &e)
                {
                    std::cout << "IPCUWorker: exception: " << e.what();
                    return true;
                }
            }

            return true;
        }))
    {
        // "IPCUWorker: cant enum dir(" ORBI_MEDIA_DIR ")");
        return;
    }
    std::cout << "max_record_id found: " << max_record_id << "\n";

    std::uint32_t file_size;

    for (uint32_t i = 0; i < 4; ++i)
    {
        std::string media_file_name = (boost::format((video ? ORBI_VIDEO_FILE_TMPL_FULL_PATH : ORBI_IMAGE_FILE_TMPL_FULL_PATH)) %
                                        max_record_id % (i + 1)).str();
        file_size = get_file_size(media_file_name);
        if (video)
            _video_sizes.push_back(file_size);
        else
            _image_sizes.push_back(file_size);
    }
}

bool CheckLast::get_resalt_video(sizes &videos)
{
    std::uint32_t tmn_size = _video_sizes.size();
    if (videos.size() != tmn_size)
        return false;

    for (std::uint32_t i = 0; i < tmn_size; ++i)
    {
        if (_video_sizes[i] != videos[i])
            return false;
    }
    return true;
}

bool CheckLast::get_resalt_image(sizes &images)
{
    std::uint32_t tmn_size = _image_sizes.size();
    if (images.size() != tmn_size)
        return false;

    for (std::uint32_t i = 0; i < tmn_size; ++i)
    {
        if (_image_sizes[i] != images[i])
            return false;
    }
    return true;
}
