#include "headers_and_constants.h"
#include <boost/format.hpp>
//#include <boost/regex.hpp>


class CheckLast
{
public:
    typedef std::vector<std::uint32_t> sizes;

    CheckLast() {}
    ~CheckLast() {}

    void check_media();
    bool get_resalt_video(sizes &videos);
    bool get_resalt_image(sizes &images);

private:

    sizes _video_sizes;
    sizes _image_sizes;
};
