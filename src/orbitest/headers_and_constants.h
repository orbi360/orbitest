#ifndef ORBI_TEST_HEADERS_AND_CONSTANTS_H
#define ORBI_TEST_HEADERS_AND_CONSTANTS_H
//system
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <stdbool.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/mman.h>
#include <ucontext.h>
#include <signal.h>
#include <execinfo.h>
#include <errno.h>
#include <stdarg.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>


//std
#include <string>
#include <cassert>
#include <iostream>
#include <fstream>
#include <map>
#include <cstring>

#include <vector>

//IPCU
extern "C"
{
#include "cmfwk_ipcu.h"
#include "cmfwk_mm.h"
#include "cmfwk_std.h"
}

using namespace std;

struct outParams{
    int ierr;
    string sout;
};

#define GOOD_RESULT "        Return 1 = 0x00000000\n        Return 2 = 0x00000000\n        Return 3 = 0x00000000\n        Return 4 = 0x00000000\n"

#define ORBI_VIDEO_DIRS_SUFFIX "ORBIV"
#define ORBI_IMAGE_DIRS_SUFFIX "ORBIP"

#define ORBI_MEDIA_FILE_PREFIX "PRIM"

#define ORBI_VIDEO_DIR_REGEXP "([0-9]{3,3})" ORBI_VIDEO_DIRS_SUFFIX
#define ORBI_IMAGE_DIR_REGEXP "([0-9]{3,3})" ORBI_IMAGE_DIRS_SUFFIX

#define ORBI_VIDEO_FILE_TMPL ORBI_MEDIA_FILE_PREFIX "%04d.MP4"
#define ORBI_IMAGE_FILE_TMPL ORBI_MEDIA_FILE_PREFIX "%04d.JPG"

#define ORBI_MEDIA_DIR "/run/RTOS/DCIM"
#define ORBI_NAND_DIR  "/run/NF1"
#define ORBI_TUNE2VID_TMPL_FILE ORBI_NAND_DIR "/T2V.TPL"
#define ORBI_TUNE2IMG_TMPL_FILE ORBI_NAND_DIR "/T2I.TPL"

#define ORBI_VIDEO_FILE_TMPL_FULL_PATH ORBI_MEDIA_DIR "/%03d" ORBI_VIDEO_DIRS_SUFFIX "/" ORBI_VIDEO_FILE_TMPL
#define ORBI_IMAGE_FILE_TMPL_FULL_PATH ORBI_MEDIA_DIR "/%03d" ORBI_IMAGE_DIRS_SUFFIX "/" ORBI_IMAGE_FILE_TMPL

#define MASK_PARAM_DATA  (0x0000000F)
#define MASK_SWITCH_CODE (0xFFFFFFF0)

#endif  //ORBI_TEST_HEADERS_AND_CONSTANTS_H
