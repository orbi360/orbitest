#include "inparser.h"
#include "working_part_of_linux.h"


//enum ID_CMDs{NO_CMD=0, CMD_INIT, CMD_WIFI, CMD_VERSION, CMD_LED,
//             CMD_BUTTONS, CMD_CALENDER, CMD_CHARGER, CMD_RECORD, CMD_TEMPERATURE, CMD_IMU, CMD_I2C, CMD_OFF, CMD_RECORDTEST
string SNAME_CMDs[N_CMDS+1] = {"nocmd","init", "wifi", "version", "led",
                               "button", "calender", "charger", "record", "temperature", "imu", "i2c", "poweroff", "recordtest"};

inParams inparser(vector<string> svec)
{

   inParams ret;
     ret.fTestlog = 0;
     ret.iTypeIO = TYPEIO_NO;
     ret.icmd = NO_CMD;
     ret.ierr = ERR_NO; //0;
       ret.fSelftestGet = 0;
       ret.fPictureVideo = 0;
       ret.fSetGet = 0;
       ret.fSoftOff = 0;

       ret.fPMICmode = 0; 
       ret.fPMICvoltage = 0;  
       ret.fPMICtest = 0;

       ret.fTemperatureAll = 0;
       ret.fShortPrint = 0;
       ret.fWifiOff = 0;
       ret.fPowerOff = 0;
       ret.fVideoLow15mb = 0;

   int vsize = svec.size();
   int iv = 0;


   // for test-log >> cout
   if(vsize>0){
       string sfirst = svec[0];
       if(sfirst == "log"){
          ret.fTestlog = 1;
          iv++;
          cout << endl << "inparser: ";
       }
   }
  
     if(iv < vsize){

      // find cmd
      string s = svec[iv++];
      ret.icmd=NO_CMD;
      for(int ic=1; ic<N_CMDS; ic++)
         if(s == SNAME_CMDs[ic]){
             ret.icmd = ID_CMDs(ic);
             ret.scmd = s;
             break;
         }

   if(ret.fTestlog){
       if(ret.icmd==NO_CMD) cout << "no_cmd!" << " ";
       else  cout << ret.scmd <<" ";
   }

      // parse operands of cmd
      switch(ret.icmd){
      default:
          //cmd is not correct
      case NO_CMD:
           ret.ierr = ERR_CMD_FALSE;
          break;

          // no operands
      case CMD_VERSION:
      case CMD_TEMPERATURE:
            ret.fTemperatureAll = 1;
      case CMD_WIFI:
          break;


          // 0 or 1 operand
      case CMD_INIT:
      case CMD_CALENDER:
      case CMD_CHARGER:
      case CMD_IMU:
      case CMD_OFF:
            //  if(iv>=vsize){
            //     ret.ierr = ERR_NOOP;
            //     break;
            if(iv < vsize){
              s = svec[iv++];
                if(s=="selftest") ret.fSelftestGet = 1;
                if(s=="softoff") ret.fSoftOff = 1;
                if(s=="gpio") ret.iTypeIO =TYPEIO_GPIO;
                if(s=="pwm") ret.iTypeIO =TYPEIO_PWM;
                if(s=="evt") ret.iTypeIO =TYPEIO_EVT;
            }

            if(ret.fTestlog){
              cout << ((ret.fSelftestGet)?"selftest":"get") << " ";
              cout << ((ret.fSoftOff)?"softoff":"pin-off") << " ";
              cout << "io_type=" << (ret.iTypeIO) << " ";
            }
          break;


          // 2 operands
      case CMD_BUTTON:
              if((iv+1)>=vsize){
                ret.ierr = ERR_NOOP;
                break;
              }
            s = svec[iv++];
              ret.ui1 = N_BUTTONS; 
              if(s!="all"){

                for (int ibtn=0; ibtn<N_BUTTONS; ibtn++)
                       if(s == BUTTON_NAMEs[ibtn]) ret.ui1 = ibtn;
                if(ret.ui1 >= N_BUTTONS) ret.ierr = ERR_OP_FALSE;

              }
            s = svec[iv++];
              ret.ui2 = strtoul(s.c_str(), NULL, 0);
              if((ret.ui2 < 1) || (ret.ui2 > 60)) ret.ierr = ERR_OP_FALSE;

            if(ret.fTestlog){
              cout << "   ui1=" <<ret.ui1 <<"  ui2=" <<ret.ui2 << " ";
            }
          break;


          // 2 operands
      case CMD_RECORD:
              if((iv+1)>=vsize){
                 ret.ierr = ERR_NOOP;
                 break;
              }
            s = svec[iv++];
              if(s=="selftest") ret.fSelftestGet = 1;
              if(s=="picture") ret.fPictureVideo = 1;
            s = svec[iv++];
              ret.ui1 = strtoul(s.c_str(), NULL, 0);
              if((ret.ui1 < 1) || (ret.ui1 > 6000)) ret.ierr = ERR_OP_FALSE;

            //ATTENTION CONST !!!
            if(ret.fSelftestGet) ret.ui1 = 2;

            if(ret.fTestlog){
              cout << ret.ui1 << " ";
            }
          break;


          // 3 operands
      case CMD_RECORDTEST:
              if((iv+3)>=vsize){
                 ret.ierr = ERR_NOOP;
                 break;
              }

             ret.ierr = ERR_NO;
           s = svec[iv++];
             ret.ui1 = strtoul(s.c_str(), NULL, 0);
           s = svec[iv++];
             ret.ui2 = strtoul(s.c_str(), NULL, 0);
           s = svec[iv++];
             ret.ui3 = strtoul(s.c_str(), NULL, 0);
           s = svec[iv++];
             ret.ui4 = 0;
             if(s.find("woff") != s.npos) ret.fWifiOff = 1;
             if(s.find("poff") != s.npos) ret.fPowerOff = 1;
             if(s.find("low") != s.npos) ret.fVideoLow15mb = 1;

          if(ret.fTestlog){
            cout << "err=" << ret.ierr << " ";
            cout << ret.ui1 << " :cnt  ";
            cout << ret.ui2 << " :video  ";
            cout << ret.ui3 << " :delay  ";
            //cout << ret.ui4 << " :wifi/power  ";
            if(ret.fWifiOff) cout << " wifi_off ";
            if(ret.fPowerOff) cout << " power_off ";
            if(ret.fVideoLow15mb) cout << " birate_low_15Mb ";

          }
          break;

          // 3/4 operands
      case CMD_I2C:
             if((iv+2)>=vsize){
                ret.ierr = ERR_NOOP;
                break;
             }
            s = svec[iv++];
             if(s=="set") ret.fSetGet = 1;

           s = svec[iv++];
             ret.ui1 = 0;
             if(s=="pmic") ret.ui1 = I2C_ADDR_PMIC;
             if(s=="calender") ret.ui1 = I2C_ADDR_CALENDER;
             if(s=="charger") ret.ui1 = I2C_ADDR_CHARGER;
             if(s=="pmic-mode"){
                ret.ui1 = I2C_ADDR_PMIC;
                ret.fPMICmode = 1;
             }   
             if(s=="pmic-volt"){
                ret.ui1 = I2C_ADDR_PMIC;
                ret.fPMICvoltage = 1;
             }   
             if(s=="pmic-test"){
                ret.ui1 = I2C_ADDR_PMIC;
                ret.fPMICtest = 1;
             }   
              if(ret.ui1==0){
                   ret.ui1 = strtoul(s.c_str(), NULL, 0); 
              }
             if(ret.ui1 > 0xff) ret.ierr = ERR_OP_FALSE;

           s = svec[iv++];
             ret.ui2 = strtoul(s.c_str(), NULL, 0);
             if(ret.ui2 > 0xff) ret.ierr = ERR_OP_FALSE;

            if(ret.fSetGet){
              if(iv>=vsize){
                ret.ierr = ERR_NOOP;
                break;
              }
              s = svec[iv++];
                ret.ui3 = strtoul(s.c_str(), NULL, 0);
                if(ret.ui3 > 0xff) ret.ierr = ERR_OP_FALSE;
            }

            if(ret.fTestlog){
              cout << "err=" << ret.ierr << " ";
              cout << ret.ui1 << " ";
              cout << ret.ui2 << " ";
              cout << ret.ui3 << " ";
              cout << ret.ui4 << " ";
            }
          break;

          // 1/4 operands
      case CMD_LED:
             if(iv>=vsize){
                ret.ierr = ERR_NOOP;
                break;
             }

            // check if first OP is known  
            ret.ierr = ERR_OP_FALSE;

            s = svec[iv];
            // first check if it is OI-type    
              if(s=="gpio"){ ret.iTypeIO =TYPEIO_GPIO; ret.ierr = ERR_NO; };
              if(s=="pwm"){ ret.iTypeIO =TYPEIO_PWM; ret.ierr = ERR_NO; };
              if(s=="evt"){ ret.iTypeIO =TYPEIO_EVT;ret.ierr = ERR_NO; };

            //if first is found - check the second
            if(ret.ierr == ERR_NO){
              iv++;
              if(iv>=vsize){
                ret.ierr = ERR_NOOP;
                break;
              }
            }

            // check if next OP is known  
            ret.ierr = ERR_OP_FALSE;
           ret.ui1=0; ret.ui2=0; ret.ui3=0; ret.ui4=0;
           s = svec[iv];
             if(s=="off"){  ret.ierr = ERR_NO; };
             if(s=="all"){ ret.ui1=ret.ui2=ret.ui3=ret.ui4=LED_MAX_VALUE; ret.ierr = ERR_NO; };
             if(s=="rr"){ ret.ui1=LED_MAX_VALUE; ret.ierr = ERR_NO; };
             if(s=="b"){ ret.ui2=LED_MAX_VALUE; ret.ierr = ERR_NO; };
             if(s=="r"){ ret.ui3=LED_MAX_VALUE; ret.ierr = ERR_NO; };
             if(s=="g"){ ret.ui4=LED_MAX_VALUE; ret.ierr = ERR_NO; };

           // if it is not-symbol OP then check 4-values
          if(ret.ierr != ERR_NO){
             if((iv+3)>=vsize){
                ret.ierr = ERR_NOOP;
                break;
             }
             ret.ierr = ERR_NO;
           s = svec[iv++];
             ret.ui1 = strtoul(s.c_str(), NULL, 0);
             if(ret.ui1 > LED_MAX_VALUE) ret.ierr = ERR_OP_FALSE;
           s = svec[iv++];
             ret.ui3 = strtoul(s.c_str(), NULL, 0);
             if(ret.ui3 > LED_MAX_VALUE) ret.ierr = ERR_OP_FALSE;
           s = svec[iv++];
             ret.ui4 = strtoul(s.c_str(), NULL, 0);
             if(ret.ui4 > LED_MAX_VALUE) ret.ierr = ERR_OP_FALSE;
           s = svec[iv++];
             ret.ui2 = strtoul(s.c_str(), NULL, 0);
             if(ret.ui2 > LED_MAX_VALUE) ret.ierr = ERR_OP_FALSE;
          }// if no-symbol OP

          if(ret.fTestlog){
            cout << "err=" << ret.ierr << " ";
            cout << ret.ui1 << " ";
            cout << ret.ui3 << " ";
            cout << ret.ui4 << " ";
            cout << ret.ui2 << " ";
          }
          break;


      }// switch icmd


   }else{

       ret.ierr = ERR_NOCMD;
   }//if-else vsize

   if(ret.ierr!=ERR_NO) ret.icmd = NO_CMD;


   if(ret.fTestlog){
        cout << "{err=" << ret.ierr << "} " << endl;
   }

   // default for IO_TYPE = EVT
   if(ret.iTypeIO==TYPEIO_NO) ret.iTypeIO=TYPEIO_EVT;

   return ret;
}

