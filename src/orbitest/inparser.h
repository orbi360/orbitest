#ifndef INPARSER_H
#define INPARSER_H

#include "headers_and_constants.h"

#define I2C_ADDR_PMIC       (0x12)
#define I2C_ADDR_CHARGER    (0x70)
#define I2C_ADDR_CALENDER   (0x68)


enum ERR_CMDs{ERR_NO=0, ERR_NOCMD, ERR_CMD_FALSE, ERR_NOOP, ERR_OP_FALSE};
enum ID_CMDs{NO_CMD=0, CMD_INIT, CMD_WIFI, CMD_VERSION, CMD_LED,
             CMD_BUTTON, CMD_CALENDER, CMD_CHARGER, CMD_RECORD, CMD_TEMPERATURE, CMD_IMU, CMD_I2C, CMD_OFF, CMD_RECORDTEST,
              N_CMDS};
enum TYPE_IO_PINS{TYPEIO_NO=0, TYPEIO_GPIO, TYPEIO_PWM, TYPEIO_EVT};

struct inParams{
    int fTestlog;
    int iTypeIO;

    ID_CMDs icmd;
    ERR_CMDs ierr;
    string scmd;

    int fSelftestGet;
    int fSetGet;
    int fPictureVideo;
    int fSoftOff;

    int fPMICmode;
    int fPMICvoltage;
    int fPMICtest; 

    int fTemperatureAll;

    int fShortPrint;
    int fWifiOff;
    int fPowerOff;
    int fVideoLow15mb;


    uint32_t ui1;
    uint32_t ui2;
    uint32_t ui3;
    uint32_t ui4;
};

inParams inparser(vector<string> svec);

#endif // INPARSER_H
