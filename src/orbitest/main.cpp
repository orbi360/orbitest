#include "inparser.h"
#include "working_part_of_linux.h"
#include "working_part_of_cm0.h"
#include "working_part_of_camera.h"

//using namespace std;

int fSignalStop;
int fVideoLocked;

void sighand(int sig){
   fSignalStop=1;
}


int main(int argc, char *argv[]){

    if(argc <2){
        cout << endl<<endl<<"orbitest version: 1.1003 + i2c-pmic-test" << endl<<endl;
//        cout << "Usage:" << endl;
        cout << "orbitest [log] CMD [op1] [op2] [op3] [op4]" << endl;
        cout << "[log]  :write additional info about errors" << endl;
        cout << "CMD: " <<"init   "<<"version   "<<"led   "<< "button   "<<"wifi   " <<"record   " << "temperature   "<< "i2c   "<< "imu" <<"calender   "<<"charger  " << endl;
        cout << "   init [gpio/pwm/evt]" <<endl;
        cout << "   version" <<endl;
        cout << "   led [gpio/pwm/evt] [all/off/rr/r/g/b];  led [rr r g b]     ex.: orbitest led all    ex2.: led 255 0 0xff 0x00" <<endl;
        cout << "   button [all/shutter/mode/wifi/power] wait_sec   ex.: orbitest button all 20" <<endl;
        cout << "   wifi" <<endl;
        cout << "   record [get/selftest] wait_sec   ex.: orbitest record get 10  ex2.: orbitest record selftest 2" <<endl;
        cout << "   record picture 1   ex.: orbitest record picture 1" <<endl;
        cout << "   recordtest count rec_time sleep_time <wifi-off power-off low15mb-bitrate>[x/woff/poff/low]  ex.: recordtest 60 60 60 woff-poff" <<endl;
        cout << "   calender [selftest]" <<endl;
        cout << "   charger [selftest]" <<endl;
        cout << "   imu [selftest]" <<endl;
        cout << "   temperature" <<endl;
        cout << "   i2c set [pmic/pmic-mode/pmic-volt/pmic-test/ calender/ charger/ 0xff(addr)] 0xff(reg) 0xff(data)   ex.: i2c set pmic-volt 236 0x0d    ex2.: i2c set pmic-test 0x0a 0x12" <<endl;
        cout << "   i2c get [pmic/pmic-mode/pmic-volt/pmic-test/ calender/ charger/ 0xff(addr)] 0xff(reg/channel)   ex.: i2c get pmic-volt" <<endl;
        cout << "   poweroff [softoff]" <<endl;
        cout <<endl;
        return 0;
    }


    fSignalStop=0;
    signal(SIGINT,sighand);

    string sRet = "app error";

    // prepared for future standalone app with terminal
    vector<string> svec;
    svec.clear();
    for(int ic=1; ic<argc; ic++)
        svec.push_back(string(argv[ic]));


//    while(!fStop){
//        cout << endl<<"#> ";

//        do{
//           string sbuff;
//           cin >> sbuff;
//           svec.push_back(sbuff);
//        }while(cin.peek()!='\n');

//        if(svec.back()=="c") svec.clear();
//        if(svec.back()=="exit") { svec.clear(); fStop=1;}

//        inParams inparams = inparser(svec);
//        ...
//    }


    inParams inparams = inparser(svec);
    outParams outparams = {0};

//TEST !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
cortex_ipcu_init();

try{

    switch(inparams.icmd){
    default:    
    case NO_CMD:
        //sRET = "no_cmd";
        outparams.sout = "no_cmd";
        outparams.ierr = 0;
        break;
    case CMD_INIT:          cmd_init(inparams, outparams);              break;
    case CMD_VERSION:       cmd_version(inparams, outparams);           break;
    case CMD_LED:           cmd_led(inparams, outparams);               break;
    case CMD_WIFI:          cmd_wifi(inparams, outparams);              break;
    case CMD_BUTTON:        cmd_button(inparams, outparams);            break;
    case CMD_CALENDER:      cmd_calender(inparams, outparams);          break;
    case CMD_TEMPERATURE:   cmd_temperature(inparams, outparams);       break;
    case CMD_IMU:           cmd_imu(inparams, outparams);               break;
    case CMD_I2C:           cmd_i2c(inparams, outparams);               break;
    case CMD_CHARGER:       cmd_charger(inparams, outparams);           break;
    case CMD_RECORD:        cmd_record(inparams, outparams);            break;
    case CMD_OFF:           cmd_poweroff(inparams, outparams);          break;

    case CMD_RECORDTEST:        cmd_recordtest(inparams, outparams);    sleep(2);        break;

    };
}catch(...)
{
   cout << endl << "ERROR is catched in main()!!" <<endl <<endl; 
}

    // if(!outparams.ierr){
    //     sRet = outparams.sout;
    // }else{
    //     sRet = "error";
    // }

    sRet = outparams.sout;
    cout << sRet <<endl;


    cortex_ipcu_close();

//    cout << "orbitest.close()" <<endl;
    return (0);
}
