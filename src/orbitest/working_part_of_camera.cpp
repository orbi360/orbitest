#include "working_part_of_camera.h"
#include "working_part_of_linux.h"
#include "working_part_of_cm0.h"
#include <bitset>

//------------Error video-photo-----------------------------------
std::map<std::uint32_t, std::string> error_rec_map =
{
		{1, "No Meida or Mount NG"},
		{2, "Proected Media"},
		{3, "Not enough space in Media"},
		{4, "Directory number is max"},
		{5, "System Error"}
};

std::map<std::uint32_t, std::string> error_sensor_map =
{
		{0, "Sensor-0 (Right Rear)"},
		{1, "Sensor-1 (Right Front)"},
		{2, "Sensor-2 (Left Rear)"},
		{3, "Sensor-3 (Left Front)"}
};
//--------------------------------------------------------------

std::vector<int> bank1 = {0, 3, 5, 6, 7, 15, 16, 17, 18, 19, 23, 25, 26, 27, 28, 40, 41, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 102, 103, 104, 105, 112, 113, 114, 116, 118, 127};
std::vector<int> bank2 = {2, 3, 4, 14, 15, 16, 20, 21, 23, 24, 26, 27, 40, 127};
std::vector<int> bank3 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 17, 18, 19, 20, 21, 82, 83, 84, 127};

static struct camera_return camera_cmd_ret_value;

extern int fSignalStop;
extern int fVideoLocked;
int fStatusThreadStop;

inParams ipLEDstatus;


GyroDataResult GyroData;

//--------Notify---------------
Error_struct notify_response;
int event_flag = 0;
uint32_t _error_code;
T_CPUCMD_NOTIFY_EVENT _rec_event;
//----------------------------

void cmd_record(inParams &inp, outParams &outp)
{
	string sresult = "";
	int err = 0;

	//count movies
	int nLargeMovies = -1;
	if (inp.fSelftestGet)
		if (!send_linux_cmd("find /run/RTOS/DCIM/ -type f -name \"PRIM*\" -size +1000000c | grep MP4 -c", sresult))	{
			nLargeMovies = stoi(sresult);
		}


    string scmd = " ";
  try{
      
	  scmd = CMD_CAMERA_MODE1;    if (inp.fTestlog) cout << scmd <<endl;
	err = send_linux_cmd(CMD_CAMERA_MODE1, sresult);
	  if(err || (sresult != GOOD_RESULT)) throw(1);

	  scmd = CMD_CAMERA_CFG1;    if (inp.fTestlog) cout << scmd <<endl;
	err = send_linux_cmd(CMD_CAMERA_CFG1, sresult);
	  if(err || (sresult != GOOD_RESULT)) throw(2);

	  scmd = CMD_CAMERA_CFG2;    if (inp.fTestlog) cout << scmd <<endl;
	err = send_linux_cmd(CMD_CAMERA_CFG2, sresult);
	  if(err || (sresult != GOOD_RESULT)) throw(3);

	  scmd = CMD_CAMERA_CFG3;    if (inp.fTestlog) cout << scmd <<endl;
	err = send_linux_cmd(CMD_CAMERA_CFG3, sresult);
	  if(err || (sresult != GOOD_RESULT)) throw(4);

	  scmd = CMD_CAMERA_CFG4;    if (inp.fTestlog) cout << scmd <<endl;
	err = send_linux_cmd(CMD_CAMERA_CFG4, sresult);
	  if(err || (sresult != GOOD_RESULT)) throw(5);

	  scmd = CMD_CAMERA_MODE2;    if (inp.fTestlog) cout << scmd <<endl;
	err = send_linux_cmd(CMD_CAMERA_MODE2, sresult);
	  if(err || (sresult != GOOD_RESULT)) throw(6);

    if (inp.fPictureVideo){
	    scmd = CMD_CAMERA_MODE3;    if (inp.fTestlog) cout << scmd <<endl;
	  err = send_linux_cmd(CMD_CAMERA_MODE3, sresult);
	    if(err || (sresult != GOOD_RESULT)) throw(7);
	}else{
	    scmd = CMD_CAMERA_MODE8;    if (inp.fTestlog) cout << scmd <<endl;
	  err = send_linux_cmd(CMD_CAMERA_MODE8, sresult);
	    if(err || (sresult != GOOD_RESULT)) throw(8);
	}

			if (inp.fTestlog){
				//cout << "   SLEEP(" <<to_string(inp.ui1) <<"s)" <<endl;
				inParams ip;
				outParams op;
				ip.fTestlog = 0;
				ip.fShortPrint = 0;
				ip.fTemperatureAll = 1;

				cout <<endl;	

				for (int isleep = inp.ui1; isleep > 0; isleep--){
					sleep(1);
					cout << (inp.ui1 - isleep) << " s";
					cmd_temperature(ip, op);
					cout << "   T= " << op.sout;
					cmd_charger(ip, op);
					cout << "   " << op.sout << endl;
				}

				cout <<endl;	

			}else{
				cout << "     sleep: " <<inp.ui1 <<endl;
				sleep(inp.ui1);
			} // if-else fTestlog


	  scmd = CMD_CAMERA_MODE2;    if (inp.fTestlog) cout << scmd <<endl;
	err = send_linux_cmd(CMD_CAMERA_MODE2, sresult);
	  if(err || (sresult != GOOD_RESULT)) throw(9);

	  scmd = CMD_CAMERA_MODE1;    if (inp.fTestlog) cout << scmd <<endl;
	err = send_linux_cmd(CMD_CAMERA_MODE1, sresult);
	  if(err || (sresult != GOOD_RESULT)) throw(10);


  }catch(int e){
			cout <<"error in:  " <<scmd <<endl <<"   err=" << err << endl;
			cout << "   " << sresult << endl;
			err = 1;
  }

	// compare number of movies before and after recording
	int nLargeMoviesAfter = -1;
	if (inp.fSelftestGet){
		if (!send_linux_cmd("find /run/RTOS/DCIM/ -type f -name \"PRIM*\" -size +1000000c | grep MP4 -c", sresult)){
			nLargeMoviesAfter = stoi(sresult);
		}
		if ((nLargeMovies < 0) || (nLargeMoviesAfter < 0)){
			cout << "The count of large movies is incorrect! Please, inform the ORBI-team!" << endl;
		}else{
			if ((nLargeMoviesAfter - nLargeMovies) != 4)
				err = 1;
		}
	} // if selftest

	if(err){
		outp.ierr = 1;
		outp.sout = "FAIL";
	}else{
		outp.ierr = 0;
		outp.sout = "OK";
	}

	// find DCIM/ -type f -name "PRIM*" -size +1M | grep MP4 -c
}

void thread_status_show()
{
	time_t tStart = time(NULL);

	ipLEDstatus.fTestlog = 0;
	ipLEDstatus.iTypeIO = TYPEIO_EVT;	
	ipLEDstatus.ui1 = ipLEDstatus.ui2 = ipLEDstatus.ui3 = ipLEDstatus.ui4 = 0;

 try{

//*** rename if necessary
	int idFilename = 1;
	int fCopied = 0;
	string sfileRecord = "/run/RTOS/record.txt";
	string scmd = "[ -f " + sfileRecord +" ] && echo 1 || echo 0";
	string sresult;

	if(send_linux_cmd(scmd.c_str(), sresult)) throw(1110);
	if(sresult.substr(0,1) =="0") fCopied = 1;

	while((!fCopied) && (idFilename<20)){
	   string sfile = "/run/RTOS/record";
	     sfile += to_string(idFilename);
		 sfile += ".txt"; 	
	   scmd = "[ -f " + sfile +" ] && echo 1 || echo 0";
	   
	   if(send_linux_cmd(scmd.c_str(), sresult)) throw(1111);

	   if(sresult.substr(0,1) =="0"){
		 string scmd2 = "cp -f " + sfileRecord + " " +sfile;  
	     if(send_linux_cmd(scmd2.c_str(), sresult)) throw(1112);
		 fCopied = 1;
	   }
       idFilename++;
	}
//***

	ofstream ofsData(sfileRecord.c_str());

	while ((!fStatusThreadStop) && (!fSignalStop)){

		this_thread::sleep_for(chrono::milliseconds(1000));


		int iloop = 0;
		while(fVideoLocked && (iloop<30)){
			this_thread::sleep_for(chrono::milliseconds(300));
			iloop++;
//			cout << "status: "<<iloop <<endl; 
//		 ofsData << "status: "<<iloop <<endl;
//		 ofsData.flush();
		}
		if(iloop >=30) throw(111);

		fVideoLocked = 1;

		inParams ip;
		outParams opt, opv;
		ip.fTestlog = 0;
		ip.fShortPrint = 1;
		ip.fTemperatureAll = 1;

		// cout << itime;
		// cmd_temperature(ip, op);
		// cout << "   \t" << op.sout;
		// cmd_charger(ip, op);
		// cout << "   \t" << op.sout << endl;

		 cmd_temperature(ip, opt);
		 cmd_charger(ip, opv);
        time_t tRead = time(NULL);

		if(ipLEDstatus.ui2 >0){
		   ipLEDstatus.ui2 = 0;
		   ipLEDstatus.ui4 = 0;
		}else{
		   ipLEDstatus.ui2 = LED_MAX_VALUE;
		   ipLEDstatus.ui4 = LED_MAX_VALUE;
		}   
//***LED
		outParams opled;
		cmd_led(ipLEDstatus, opled);
//***

		fVideoLocked = 0;

         double dt = difftime(tRead, tStart);
		char ss[200];
		 sprintf(ss,"%.0f   %s   %s", dt, opt.sout.c_str(), opv.sout.c_str());
		 cout << ss <<endl;

		 ofsData << ss << endl;
		 ofsData.flush();

	}
	 ofsData.close();

 }catch(...){
		cout << "error in status-thread.." << endl;
		fStatusThreadStop = 1;
		system("echo status-thread-error >> /run/RTOS/error.txt");
 }
 
}

void cmd_recordtest(inParams &inp, outParams &outp)
{
	outp.ierr = 1;
	outp.sout = "FAIL";

	fStatusThreadStop = 0;
	fVideoLocked = 0;
	 thread th_status_show(thread_status_show);
	 th_status_show.detach();

	 sleep(10);

	string sresult = "";
	int err = 0;

  try{

// *** pmic-adjustmens
  // if(inp.fPMICtest){
	// 	inParams ipt;
	// 	outParams opt;
  //     ipt.fSetGet = 1;
	// 	  ipt.ui1 = 0x12;  // pmic
	// 	  ipt.ui2 = 0x12;  // dcdc5 - NRM_volt
	// 		ipt.ui3 = 0x11;  // 1.2+0.15V, default=0x10
	// 	cmd_i2c(ipt, opt);
	// 	  ipt.ui2 = 0x19;  // dcdc5 - SLP_volt
	// 		ipt.ui3 = 0x11;  // 1.2+0.15V, default=0x10
	// 	cmd_i2c(ipt, opt);
	// 	  ipt.ui2 = 0x0c;  // config - dc5[nrm-slp]+dc6[nrm-slp]
	// 		ipt.ui3 = 0x22;  // not sleep for 5, defaultDVT=0x32
	// 	cmd_i2c(ipt, opt);

	// }


//*** WiFi stop 
  if(inp.fWifiOff){
//		system("/bin/usr/orbi/stop-wifi-ap.sh");
		err = send_linux_cmd("/usr/bin/orbi/stop-wifi-ap.sh", sresult); 
		if(err) throw(222);
	}  
//*** 

	err = send_linux_cmd(CMD_CAMERA_MODE1, sresult);
	if(err || (sresult != GOOD_RESULT)) throw(1);
	err = send_linux_cmd(CMD_CAMERA_CFG1, sresult);
	if(err || (sresult != GOOD_RESULT)) throw(2);
	
	if(inp.fVideoLow15mb) err = send_linux_cmd(CMD_CAMERA_CFG2low, sresult);
	else err = send_linux_cmd(CMD_CAMERA_CFG2, sresult);
	if(err || (sresult != GOOD_RESULT)) throw(3);
	
	err = send_linux_cmd(CMD_CAMERA_CFG3, sresult);
	if(err || (sresult != GOOD_RESULT)) throw(4);
	err = send_linux_cmd(CMD_CAMERA_CFG4, sresult);
	if(err || (sresult != GOOD_RESULT)) throw(5);

	int fstop = 0;
	int cntRec = 0;
	while ((!fstop)&&(!fSignalStop)){

		int iloop=0;
		while(fVideoLocked){
//			this_thread::sleep_for(chrono::milliseconds(100));
		  sleep(1);
		  iloop++;
		  if(iloop>20) throw(333);
//			cout << "      record: "<<iloop <<endl; 
		}
		fVideoLocked = 1;

		err = send_linux_cmd(CMD_CAMERA_MODE2, sresult);
		if(err || (sresult != GOOD_RESULT)) throw(6);

		ipLEDstatus.ui1 = LED_MAX_VALUE;

		err = send_linux_cmd(CMD_CAMERA_MODE8, sresult);
		if(err || (sresult != GOOD_RESULT)) throw(7);

		fVideoLocked = 0;
		sleep(inp.ui2);
		cntRec++;

		while(fVideoLocked){
			this_thread::sleep_for(chrono::milliseconds(100));
		}
		fVideoLocked = 1;

		err = send_linux_cmd(CMD_CAMERA_MODE2, sresult);
		if(err || (sresult != GOOD_RESULT)) throw(8);

		ipLEDstatus.ui1 = 0;


		err = send_linux_cmd(CMD_CAMERA_MODE1, sresult);
		if(err || (sresult != GOOD_RESULT)) throw(9);

		fVideoLocked = 0;
		sleep(inp.ui3);

		if (cntRec >= inp.ui1)
			fstop = 1;

	} // while cnt

  }catch(int e){
	cout << endl << "FAIL-case: "<< e << endl;  
	fStatusThreadStop = 1;
    err=1;
//		system("echo record_test-error >> /run/RTOS/error.txt");
		ofstream ofsError("/run/RTOS/errors.txt");
		ofsError << "recordtest-error:  "<< e <<endl;
		ofsError.flush();
		ofsError.close(); 
  }

  fStatusThreadStop = 1;

  if(!err){ 
	outp.ierr = 0;
	outp.sout = "OK";
  }else{
	outp.ierr = 1;
	outp.sout = "FAIL";
  }	

//*** PowerOFF 
    if(inp.fPowerOff){
		cout << outp.sout <<endl;
		sleep(1);

		inParams ipp;
		outParams opp;
		ipp.fTestlog = 0;
		ipp.fSoftOff = 0;

		 cmd_poweroff(ipp, opp);
	}  
//*** 

	// find DCIM/ -type f -name "PRIM*" -size +1M | grep MP4 -c
}

static void camera_cmd_rcv_callback(UINT8 id,
									UINT32 pdata,
									UINT32 length,
									UINT32 cont,
									UINT32 total_length)
{
	T_CPUCMD_COMMAND_ACK ack;

	if (camera_cmd_rcv_id != id ||
		length != sizeof(ack) ||
		length != total_length ||
		cont == D_CONTINUE)
	{
		camera_cmd_ret_value.ret = E_CAMERA_IF_RES_CODE_ERROR;
		camera_cmd_complete_flag = 1;
		printf("Receive response error.\n");
		return;
	}

	memcpy(&ack, (void *)pdata, sizeof(ack));

	if (ack.t_head.magic_code != D_CPU_IF_MCODE_COOMNAD_ACK ||
		ack.t_head.format_version != D_CPU_IF_COM_FORMAT_VERSION ||
		ack.t_head.cmd_set != camera_current_cmd_set ||
		ack.t_head.cmd != camera_current_cmd)
	{
		camera_cmd_ret_value.ret = E_CAMERA_IF_RES_CODE_ERROR;
		camera_cmd_complete_flag = 1;
		printf("Receive response error. code=0x%08X, version=0x%08X, cmd_set=0x%08X, cmd=0x%08X\n",
			   ack.t_head.magic_code, ack.t_head.format_version,
			   ack.t_head.cmd_set, ack.t_head.cmd);
		return;
	}

	camera_cmd_ret_value.ret = (E_CAMERA_IF_RES_CODE)ack.ret1;
	camera_cmd_ret_value.ret_code = ack.ret2;
	camera_cmd_ret_value.ret_param3 = ack.ret3;
	camera_cmd_complete_flag = 1;
	/*printf("Receive response.\n");
	printf("        Return 1 = 0x%08X\n", ack.ret1);
	printf("        Return 2 = 0x%08X\n", ack.ret2);
	printf("        Return 3 = 0x%08X\n", ack.ret3);
	printf("        Return 4 = 0x%08X\n", ack.ret4);*/
	return;
}

int camera_ipcu_init(void)
{
	INT32 ret;

	/* Initialize IPCU */
	ret = FJ_IPCU_Open(E_FJ_IPCU_MAILBOX_TYPE_1, (UINT8 *)&camera_cmd_send_id);
	if (ret != FJ_ERR_OK)
		goto send_err;
	ret = FJ_IPCU_Open(E_FJ_IPCU_MAILBOX_TYPE_0, (UINT8 *)&camera_cmd_rcv_id);
	if (ret != FJ_ERR_OK)
		goto recv_err;
	ret = FJ_IPCU_SetReceiverCB(camera_cmd_rcv_id, camera_cmd_rcv_callback);
	if (ret != FJ_ERR_OK)
		goto cam_if_cb_err;

	return 0;

cam_if_cb_err:
	FJ_IPCU_Close(camera_cmd_rcv_id);
recv_err:
	FJ_IPCU_Close(camera_cmd_send_id);
	camera_cmd_rcv_id = 0xFF;
send_err:
	camera_cmd_send_id = 0xFF;
	return -1;
}

int notify_ipcu_init(void)
{
	INT32 ret;

	ret = FJ_IPCU_Open(E_FJ_IPCU_MAILBOX_TYPE_4, (UINT8 *)&notify_cmd_id);
	if (ret != FJ_ERR_OK)
		goto open_err_notify;

	ret = FJ_IPCU_SetReceiverCB(notify_cmd_id, cmd_notify_callback);
	if (ret != FJ_ERR_OK)
		goto open_err_notify;

	memset(&notify_response, -1, sizeof notify_response);

	return 0;

open_err_notify: 
	FJ_IPCU_SetReceiverCB(notify_cmd_id, NULL);
	FJ_IPCU_Close(notify_cmd_id);

	return -1;
}

void camera_ipcu_close(void)
{
	if (camera_cmd_send_id != 0xFF){
		FJ_IPCU_Close(camera_cmd_send_id);
	}	
	if (camera_cmd_rcv_id != 0xFF){
		FJ_IPCU_Close(camera_cmd_rcv_id);
	}

	camera_cmd_send_id = 0xFF;
	camera_cmd_rcv_id = 0xFF;
}

void notify_ipcu_close(void)
{
	if (notify_cmd_id != 0xFF)
	{
		FJ_IPCU_SetReceiverCB(notify_cmd_id, NULL);
		FJ_IPCU_Close(notify_cmd_id);
		notify_cmd_id = 0xFF;
	}
}

static int camera_send_cmd(E_CAM_IF_COM_SET cmd_set, E_CAM_IF_SUB_COM cmd, ULONG param1 = 0xFFFFFFFF, ULONG param2 = 0xFFFFFFFF, ULONG param3 = 0xFFFFFFFF, ULONG param4 = 0xFFFFFFFF)
{
	INT32 ret;
	T_CPUCMD_COMMAND_REQUEST cmd_req;
	UINT32 try_cnt = 0;
	UCHAR bTry_error = 0;
	struct camera_return tmp_tst;
	UINT32 vaddr;
	CHAR *string_addr = (CHAR *)FJ_MM_phys_to_virt(D_TEST_LINUX_RTOS_COMMON_STRING_BUF);
	(void)string_addr;

	vaddr = FJ_MM_phys_to_virt(D_TEST_LINUX_RTOS_COMMON_STRING_BUF);
	if (vaddr == (UINT32)MAP_FAILED)
	{
		printf("FJ_MM_phys_to_virt error\n");
		return -3;
	}

	camera_cmd_complete_flag = 0;
	memset(&tmp_tst, 0x00, sizeof(tmp_tst));
	camera_cmd_ret_value = tmp_tst;
	camera_current_cmd_set = cmd_set;
	camera_current_cmd = cmd;
	cmd_req.t_head.magic_code = D_CPU_IF_MCODE_COMMAND_REQ;
	cmd_req.t_head.format_version = D_CPU_IF_COM_FORMAT_VERSION;
	cmd_req.t_head.cmd_set = cmd_set;
	cmd_req.t_head.cmd = cmd;

	cmd_req.param1 = param1;
	cmd_req.param2 = param2;
	cmd_req.param3 = param3;
	cmd_req.param4 = param3;

	ret = FJ_IPCU_Send(camera_cmd_send_id, (UINT32)&cmd_req, sizeof(cmd_req), IPCU_SEND_SYNC);
	if (ret != FJ_ERR_OK)
	{
		FJ_IPCU_Close(camera_cmd_send_id);
		return -1;
	}

	while (1)
	{
		if (camera_cmd_complete_flag == 1)
		{
			break;
		}

		if (try_cnt > 30)
		{
			bTry_error = 1;
			break;
		}
		++try_cnt;
		sleep(1);
	}

	if (bTry_error == 1)
		return -2;
	return 0;
}

int ipcu_record()
{
	ULONG param[4];

	//set idle mode
	param[0] = 0x01;
	if (camera_send_cmd(E_CAM_IF_COM_SET_MODE_CHANGE, E_CAM_IF_SUB_COM_CHANGE_MODE, param[0]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}

	//set frame size and record mode (32)
	param[0] = 32;
	param[1] = 5;
	param[2] = 1;
	param[3] = 0;
	if (camera_send_cmd((E_CAM_IF_COM_SET)0x00010300, (E_CAM_IF_SUB_COM)0x00000100, param[0], param[1], param[2], param[3]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}

	//set bitrate
	param[0] = 0;
	param[1] = 15000000;
	param[2] = 15000000;
	param[3] = 0;
	if (camera_send_cmd((E_CAM_IF_COM_SET)0x00010300, (E_CAM_IF_SUB_COM)0x00000200, param[0], param[1], param[2], param[3]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}

	//set audio enable
	param[0] = 0x01;
	if (camera_send_cmd((E_CAM_IF_COM_SET)0x00010200, (E_CAM_IF_SUB_COM)0x00000500, param[0]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}

	//set audio mic selection
	param[0] = 0x01;
	if (camera_send_cmd((E_CAM_IF_COM_SET)0x00010200, (E_CAM_IF_SUB_COM)0x00000600, param[0]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}

	//set preview mode
	param[0] = 0x02;
	if (camera_send_cmd(E_CAM_IF_COM_SET_MODE_CHANGE, E_CAM_IF_SUB_COM_CHANGE_MODE, param[0]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}

	//set record mode
	param[0] = 0x08;
	if (camera_send_cmd(E_CAM_IF_COM_SET_MODE_CHANGE, E_CAM_IF_SUB_COM_CHANGE_MODE, param[0]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}

	sleep(1);

	//set preview mode
	param[0] = 0x02;
	if (camera_send_cmd(E_CAM_IF_COM_SET_MODE_CHANGE, E_CAM_IF_SUB_COM_CHANGE_MODE, param[0]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}

	//set idle mode
	param[0] = 0x01;
	if (camera_send_cmd(E_CAM_IF_COM_SET_MODE_CHANGE, E_CAM_IF_SUB_COM_CHANGE_MODE, param[0]) != 0)
	{
		//\nPrint error send command \n
		return 1;
	}
	return 0;
}

int read_gyro_file(string const &filepatch)
{
	ifstream gyro_file(filepatch, std::ios::binary | std::ios::in);
	if (!gyro_file.good())
	{
		cout << ("Error open file");
		return 0;
	}

	int i = 0;
	GyroDataStruct temp;
	AllCam_OneFrame temp_cam;

	cout << ("start  read file") << endl;

	while (gyro_file.read((char *)&temp, sizeof(temp)))
	{
		temp_cam.OneFrame[i] = temp;
		i++;
		if (i == COUNT_CAM)
		{
			GyroData.AllFrame.push_back(temp_cam);
			i = 0;
		}
	};

	gyro_file.close();

	return 1;
}

void debugread_gyro_information()
{
	for (auto i : GyroData.AllFrame)
	{
		for (int n = 0; n < COUNT_CAM; n++)
		{
			cout << std::hex << i.OneFrame[n].assemblyImageIndex << " ";
			cout << std::hex << i.OneFrame[n].asseblyLayoutNbColumns << " ";
			cout << std::hex << i.OneFrame[n].asseblyLayoutNbLines << " ";

			cout << std::hex << i.OneFrame[n].ColorFormat << " ";
			cout << std::hex << i.OneFrame[n].CorrectionLevel << " ";

			cout << std::hex << i.OneFrame[n].CameraFixedOrientation << " ";
			cout << std::hex << i.OneFrame[n].RPL << " ";
			cout << std::hex << i.OneFrame[n].reserved << " ";

			cout << std::hex << i.OneFrame[n].WidthImagesource << " ";
			cout << std::hex << i.OneFrame[n].HeightImageSource << " ";
			cout << std::hex << i.OneFrame[n].Rotation << " ";
			cout << std::hex << i.OneFrame[n].Center_X << " ";
			cout << std::hex << i.OneFrame[n].Center_Y << " ";
			cout << std::hex << i.OneFrame[n].Width << " ";
			cout << std::hex << i.OneFrame[n].Height << " ";

			cout << std::hex << i.OneFrame[n].CalibrationRotation_Pan << " ";
			cout << std::hex << i.OneFrame[n].CalibrationRotation_Tilt << " ";
			cout << std::hex << i.OneFrame[n].CalibrationRotation_Roll << " ";

			cout << std::hex << i.OneFrame[n].CalibrationTranslation_X << " ";
			cout << std::hex << i.OneFrame[n].CalibrationTranslation_Y << " ";
			cout << std::hex << i.OneFrame[n].CalibrationTranslation_Z << " ";

			cout << std::hex << i.OneFrame[n].Quaternion_W << " ";
			cout << std::hex << i.OneFrame[n].Quaternion_X << " ";
			cout << std::hex << i.OneFrame[n].Quaternion_Y << " ";
			cout << std::hex << i.OneFrame[n].Quaternion_Z << " ";
			cout << std::hex << i.OneFrame[n].Accel_X << " ";
			cout << std::hex << i.OneFrame[n].Accel_Y << " ";
			cout << std::hex << i.OneFrame[n].Accel_Z << " ";
			cout << std::hex << i.OneFrame[n].Gyro_X << " ";
			cout << std::hex << i.OneFrame[n].Gyro_Y << " ";
			cout << std::hex << i.OneFrame[n].Gyro_Z << " ";
			cout << std::hex << i.OneFrame[n].Compass_X << " ";
			cout << std::hex << i.OneFrame[n].Compass_Y << " ";
			cout << std::hex << i.OneFrame[n].Compass_Z << " ";

			cout << std::hex << i.OneFrame[n].CameraRotation_Pan << " ";
			cout << std::hex << i.OneFrame[n].CameraRotation_Tilt << " ";
			cout << std::hex << i.OneFrame[n].CameraRotation_Roll << " ";

			cout << std::hex << i.OneFrame[n].Debug << " ";

			cout << "\n"
				 << "NEXT Sensor"
				 << "\n";
		}
		cout << "\n"
			 << "Next FRAME"
			 << "\n";
	}
}

void cmd_notify_callback(UINT8 id, UINT32 pdata, UINT32 length, UINT32 cont, UINT32 total_length)
{
	if (notify_cmd_id != id || length != sizeof(T_CPUCMD_NOTIFY_EVENT) || length != total_length || cont == D_CONTINUE)
	{
		std::cout << "IPCU: notify callback error id=" << id << ", length=" << length << ", rcv_id=" << notify_cmd_id << ", size=" << sizeof(T_CPUCMD_NOTIFY_EVENT) << std::endl;

		return;
	}

	memcpy((void *)&(_rec_event), (void *)pdata, sizeof(T_CPUCMD_NOTIFY_EVENT));

	if (_rec_event.t_head.magic_code != D_CPU_IF_MCODE_EVENT_NOTIFY || _rec_event.t_head.format_version != D_CPU_IF_COM_FORMAT_VERSION)
	{
		std::cout << "IPCU: event head error!" << std::endl;

		return;
	}

	switch (_rec_event.t_head.cmd_set)
	{
	case E_CAM_IF_COM_SET_NOTIFY_EVENT_STATUS_CHANGED:
		std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
		std::cout << "IPCU: notify Parameter1=0x" << std::hex << _rec_event.notify1 << std::endl;
		if (_rec_event.t_head.cmd == E_CAM_IF_SUB_COM_NOTIFY_EVENT_SD_STATUS_CHANGED)
		{
			std::cout << "SD status changed" << std::endl;
		}
		break;

	case E_CAM_IF_COM_SET_NOTIFY_EVENT_PROC_FINISH:
		std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
		switch (_rec_event.t_head.cmd)
		{
		case E_CAM_IF_SUB_COM_NOTIFY_EVENT_IMAGE_CAPTURE_FINISH:
			std::cout << "IPCU: notify(image) Parameter1=0x" << std::hex << _rec_event.notify1 << std::endl;
			break;

		case E_CAM_IF_SUB_COM_NOTIFY_EVENT_VIDEO_CAPTURE_FINISH:
			std::cout << "IPCU: notify(video) Parameter1=0x" << std::hex << _rec_event.notify1 << ", Parameter2=0x" << _rec_event.notify2 << std::endl;
			break;

		default:
			std::cout << "IPCU: notify Parameter1=0x" << std::hex << _rec_event.notify1 << ", Parameter2=0x" << _rec_event.notify2 << std::endl;
			break;
		}
		break;

	case E_CAM_IF_COM_SET_NOTIFY_EVENT_PROC_ERROR:
		switch (_rec_event.t_head.cmd)
		{
		case E_CAM_IF_SUB_COM_NOTIFY_EVENT_IMAGE_CAPTURE_ERROR:
			if (notify_response.image_capture_error[_rec_event.notify1 - 1] == -1)
			{
				std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
				std::cout << "IPCU: notify( image recording error ) Parameter1=0x" << std::hex << _rec_event.notify1 << std::endl;
				notify_response.image_capture_error[_rec_event.notify1 - 1] = _rec_event.notify1;
				_error_code |= 0x320 | _rec_event.notify1;
			}
			break;

		case E_CAM_IF_SUB_COM_NOTIFY_EVENT_VIDEO_CAPTURE_ERROR:
			if (notify_response.video_recording_error[_rec_event.notify1 - 1] == -1)
			{
				std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
				std::cout << "IPCU: notify( video recording error ) Parameter1=0x" << std::hex << _rec_event.notify1 << std::endl;
				notify_response.video_recording_error[_rec_event.notify1 - 1] = _rec_event.notify1;
				_error_code |= 0x330 | _rec_event.notify1;
			}
			break;

		case E_CAM_IF_SUB_COM_NOTIFY_EVENT_SENSOR_I2C_ERROR:
			if (notify_response.sensor_i2c_error[_rec_event.notify1] == -1)
			{
				std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
				std::cout << "IPCU: notify( sensor i2c error ) Parameter1=0x" << std::hex << _rec_event.notify1 << std::endl;
				notify_response.sensor_i2c_error[_rec_event.notify1] = _rec_event.notify1;
				_error_code |= 0x370 | _rec_event.notify1;
			}
			break;

		case E_CAM_IF_SUB_COM_NOTIFY_EVENT_SENSOR_MIPI_DATA_ERROR:
			if (notify_response.sensor_MIPI_data_error[_rec_event.notify1] == -1)
			{
				std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
				std::cout << "IPCU: notify( sensor(MIPI) data error ) Parameter1=0x" << std::hex << _rec_event.notify1 << std::endl;
				notify_response.sensor_MIPI_data_error[_rec_event.notify1] = _rec_event.notify1;
				_error_code |= 0x380 | _rec_event.notify1;
			}
			break;

		case E_CAM_IF_SUB_COM_NOTIFY_EVENT_GYRO_ERROR:
			if (notify_response.gyro_error[_rec_event.notify1 - 1] == -1)
			{
				std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
				std::cout << "IPCU: notify( gyro error ) Parameter1=0x" << std::hex << _rec_event.notify1 << std::endl;
				notify_response.gyro_error[_rec_event.notify1 - 1] = _rec_event.notify1;
				_error_code |= 0x3A0 | _rec_event.notify1;
			}
			break;

		default:
			std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
			std::cout << "IPCU: notify(unknow) Parameter1=0x" << std::hex << _rec_event.notify1 << ", Parameter2=0x" << _rec_event.notify2 << std::endl;
			_error_code |= 0x999;
			break;
		}

		break;

	case E_CAM_IF_COM_SET_NOTIFY_EVENT_EDID_RECEIVED:
		std::cout << "IPCU: notify Command=0x" << std::hex << _rec_event.t_head.cmd_set << ", Sub-Command=0x" << _rec_event.t_head.cmd << std::endl;
		std::cout << "IPCU: notify Parameter1=0x" << std::hex << _rec_event.notify1 << ", Parameter2=0x" << _rec_event.notify2 << ", Parameter3=0x" << std::hex << _rec_event.notify3 << ", Parameter4=0x" << _rec_event.notify4 << std::endl;
		break;

	default:
		std::cout << "IPCU: event command error" << std::endl;
		break;
	}

	event_flag = 1;
	//decoding_notify();
}

void decoding_notify() // maybe this function to thread for monitoring status callback???
{
	//while (true)
	//{
	if (event_flag)
	{
		std::cout << "!!!Fatal error!!!" << std::endl;

		switch (_error_code & MASK_SWITCH_CODE)
		{
		case 0x320:
		case 0x330:
			std::cout << std::hex << _error_code << " " << (_error_code & MASK_PARAM_DATA) << " " << error_rec_map[_error_code & MASK_PARAM_DATA] << std::endl;
			break;
		case 0x370:
		case 0x380:
			std::cout << std::hex << _error_code << " " << (_error_code & MASK_PARAM_DATA) << " " << error_sensor_map[_error_code & MASK_PARAM_DATA] << std::endl;
			break;
		case 0x3A0:
			std::cout << std::hex << _error_code << " " << (_error_code & MASK_PARAM_DATA) << " " << ((_error_code & MASK_PARAM_DATA) == 1 ? "Communication error" : "Device(Data) error") << std::endl;
			break;
		default:
			std::cout << std::hex << _error_code << " " << (_error_code & MASK_PARAM_DATA) << " "
					  << "unknow error code" << std::endl;
			break;
		}

		event_flag = 0;
	}
	//}
}

int open_device(ULONG device_number)
{
	if(camera_ipcu_init() != 0)  return 1;
//	   cout << " Open Device number = " << device_number << endl;

	//set idle mode
	if (camera_send_cmd(E_CAM_IF_COM_SET_MODE_CHANGE, E_CAM_IF_SUB_COM_CHANGE_MODE, 0x01) != 0)
	{
		return 2;
	}

	//Device Open gyro
	if (camera_send_cmd((E_CAM_IF_COM_SET)0x00040300, (E_CAM_IF_SUB_COM)0x00000400, device_number) != 0)
	{
		return 3;
	}

//	std::cout << "\n Open Device number =  " << device_number << " --> COMPLETE" << std::endl;
	return 0;
}

int close_device(ULONG device_number)
{
//	std::cout << "\n Close Device number = " << device_number << std::endl;
	//Device Open gyro
	if (camera_send_cmd((E_CAM_IF_COM_SET)0x00040300, (E_CAM_IF_SUB_COM)0x00000500, device_number) != 0)
	{
//		printf(" \nPrint error send command \n");
		return 1;
	}

	//set idle mode
	if (camera_send_cmd(E_CAM_IF_COM_SET_MODE_CHANGE, E_CAM_IF_SUB_COM_CHANGE_MODE, 0x01) != 0)
	{
//		printf(" \nPrint error send command \n");
		return 2;
	}

	camera_ipcu_close();
//	std::cout << "\n Close Device number =  " << device_number << " --> COMPLETE" << std::endl;

	return 0;
}

int write_registr_imu(uint8_t reg, uint8_t data)
{
	ULONG lreg = reg;
	ULONG ldata = data;
	if (camera_send_cmd((E_CAM_IF_COM_SET)0x00040300, (E_CAM_IF_SUB_COM)0x00000700, 0x00000008, lreg, ldata) != 0)
	{
//		printf(" \nPrint error send command \n");
		return 1;
	}

	return 0;
}

int read_registr_imu(uint8_t reg, uint8_t& data)
{
	ULONG lreg = reg;
	if (camera_send_cmd((E_CAM_IF_COM_SET)0x00040300, (E_CAM_IF_SUB_COM)0x00000600, 0x00000008, lreg) != 0)
	{
//		printf(" \nPrint error send command \n");
		return 1;
	}
	data = camera_cmd_ret_value.ret_param3;
	return 0;
}


void cmd_imu(inParams& inp, outParams& outp)
{
    int err = 0;
    char ss[200];
	int resSelftest = 0;

    try{
		err = open_device(ULONG(DeviceID::Gyro));
		  if(err) {sprintf(ss,"open_error id=%ld   err=%d", ULONG(DeviceID::Gyro), err); throw 1;}

		uint8_t ireg = 0x0;
		uint8_t idat = 0;
		int ia_x=0, ia_y=0, ia_z=0;
		int iT=0;
		double scl_a = 4./32768.;
		double sign_a = 1.;
		double va_x, va_y, va_z;
		double vT;

/*
         ireg = 0x03;
		 idat = 0x30;
		 err = write_registr_imu(ireg, idat);
		   if(err) {sprintf(ss,"write_error   err=%d", err); throw 3;}
		 sleep(1);  

        ireg = 0x00;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x03;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x05;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x06;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x07;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
//********
        ireg = 0x7F;
		idat = 0x2<<4;
		err = write_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"write_error   err=%d", err); throw 3;}

        ireg = 0x10;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get2: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x11;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get2: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x12;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get2: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x13;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get2: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x14;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get2: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;
        ireg = 0x15;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get2: 0x%02x = 0x%02x", ireg, idat);
		  cout << ss <<endl;

        ireg = 0x7F;
		idat = 0x0<<4;
		err = write_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"write_error   err=%d", err); throw 3;}
//*************


        ireg = 0x2D;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  ia_x += (idat & 0xFF)<<8;
        ireg++;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  ia_x += idat & 0xFF;
    		// convert sign
			sign_a = 1.;
		    if(ia_x &0x8000){
		      ia_x = (~ia_x)+0x01;
			  sign_a=-1.;
		    }
		  va_x = ia_x*scl_a*sign_a;	


        ireg++;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  ia_y += (idat & 0xFF)<<8;
        ireg++;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  ia_y += idat & 0xFF;
    		// convert sign
			sign_a = 1.;
		    if(ia_y &0x8000){
		      ia_y = (~ia_y)+0x01;
			  sign_a=-1.;
		    }
		  va_y = ia_y*scl_a*sign_a;	

        ireg++;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  ia_z += (idat & 0xFF)<<8;
        ireg++;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  ia_z += idat & 0xFF;
    		// convert sign
			sign_a = 1.;
		    if(ia_z &0x8000){
		      ia_z = (~ia_z)+0x01;
			  sign_a=-1.;
		    }
		  va_z = ia_z*scl_a*sign_a;	

        ireg=0x39;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  iT += (idat & 0xFF)<<8;
        ireg++;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  iT += idat & 0xFF;
		  vT = iT*1./333.87;

		sprintf(ss,"imu-T: %.1fC", vT);
		  cout << ss <<endl;

		sprintf(ss,"acc: x= %.3f y= %.3f z= %.3f", va_x, va_y, va_z);
//		  cout << ss <<endl;
*/

        ireg = 0x00;
		err = read_registr_imu(ireg, idat);
		  if(err) {sprintf(ss,"read_error   err=%d", err); throw 3;}
		  sprintf(ss,"imu-get: 0x%02x = 0x%02x", ireg, idat);

    	if(inp.fSelftestGet){
			if(idat==0xEA) resSelftest = 1;	
		}	


		err = close_device(ULONG(DeviceID::Gyro));
		  if(err) {sprintf(ss,"close_error   err=%d", err); throw 2;}

	}catch(...){
		if (inp.fTestlog){
		   cout << ss <<endl;	
		}	 
	}

    if(err){
        outp.ierr = 1;
        outp.sout = "FAIL";
	}else{
		outp.ierr = 0;
		outp.sout = string(ss);
		if(inp.fSelftestGet){
			if(resSelftest){
				outp.sout = "OK";	
			}else{
				outp.sout = "FAIL";
			}
		}
	} 


/*
    if(inp.fSelftestGet){
        double vsec1 = 0, vsec2 = 0;
        vsec1 = 60.*60.*g_cortex_ret.data[5] + 60.* g_cortex_ret.data[4] + g_cortex_ret.data[3];

        if(!err){
            sleep(5);
            err = ipcu_get_calendar_data();
           if(!err){
            if (inp.fTestlog){
                for(int i=0; i<LENGTH_CORTEX_CMD; i++)
                    cout << (to_string((g_cortex_ret.data[i]) &0xffffffff) + "  ");
                cout <<endl;
            } 
                outp.ierr = 0;
           }else{
                outp.ierr = 1;
                outp.sout = "FAIL";
           }
        }
       
      if(!outp.ierr){
        vsec2 = 60.*60.*g_cortex_ret.data[5] + 60.* g_cortex_ret.data[4] + g_cortex_ret.data[3];
        if(vsec2-vsec1 >= 4){
            outp.sout = "OK";
        }else{
            outp.ierr = 1;
            outp.sout = "FAIL";
        }    
      }
    }
*/
}
