#ifndef ORBI_TEST_WORKING_PART_OF_CAMERA_H
#define ORBI_TEST_WORKING_PART_OF_CAMERA_H


#include "headers_and_constants.h"
#include "inparser.h"

#define COUNT_CAM (4)
#define ERROR_CODE_COUNT (5)

enum class DeviceID : uint16_t
{
	Sensor_0 = 0,
	Sensor_1,
	Sensor_2,
	Sensor_3,
	EEPROM_0,
	EEPROM_1, 
	EEPROM_2, 
	EEPROM_3,
	Gyro,
	MAX_DEVICEID
};

struct GyroDataStruct
{
///Basic information
    unsigned int assemblyImageIndex;
    unsigned int asseblyLayoutNbColumns;
    unsigned int asseblyLayoutNbLines;

    unsigned long ColorFormat;
    unsigned long CorrectionLevel;

    unsigned int CameraFixedOrientation;
    char RPL[6];
	char reserved[2];

///Sensor Unique Information
    unsigned int WidthImagesource;
    unsigned int HeightImageSource;
    float Rotation;
    float Center_X;
    float Center_Y;
    float Width;
    float Height;

///Calibration Information
    float CalibrationRotation_Pan;
    float CalibrationRotation_Tilt;
    float CalibrationRotation_Roll;

    float CalibrationTranslation_X;
    float CalibrationTranslation_Y;
    float CalibrationTranslation_Z;

///DMP RAW Information
	float Quaternion_W;
	float Quaternion_X;
	float Quaternion_Y;
	float Quaternion_Z;
	float Accel_X;
	float Accel_Y;
	float Accel_Z;
	float Gyro_X;
	float Gyro_Y;
	float Gyro_Z;
	float Compass_X;
	float Compass_Y;
	float Compass_Z;

///Camera Rotation Information
    float CameraRotation_Pan;
    float CameraRotation_Tilt;
    float CameraRotation_Roll;

	unsigned long Debug;
};

struct AllCam_OneFrame
{
	GyroDataStruct OneFrame[COUNT_CAM];
};

typedef struct GyroDataResult
{
	vector<AllCam_OneFrame> AllFrame;
}GyroDataResult;


#define D_TEST_LINUX_RTOS_COMMON_STRING_BUF		(0x4FE40028)
#define D_TEST_LINUX_RTOS_COMMON_STRING_BUF_SIZE	(256)

#define CAMERA_IF_CMDREQ_MAX (sizeof(camera_if_req) / sizeof(camera_if_cmd_request_data))

 //communication control
struct camera_return {
	E_CAMERA_IF_RES_CODE ret;
	ULONG ret_code;
	ULONG ret_param3;
};

struct msgtype {
	ULONG mtype;
	T_CPUCMD_STREAM msg_stream;
};

typedef struct {
	E_CAM_IF_COM_SET cmd_set;
	E_CAM_IF_SUB_COM cmd;
	ULONG param[4];
	CHAR *param_str[4];
} cmd_req_pack;

//command information
typedef struct {
	const char *cmd_str;
	ULONG cmd_set;
	ULONG cmd;
	ULONG param[4];
} camera_cmd_request_data;

static volatile UINT8 camera_cmd_send_id = 0xFF;
static volatile UINT8 camera_cmd_rcv_id = 0xFF;

//---------------Notify------------------------------------------------
static volatile UINT8 notify_cmd_id = 0xFF;
#define E_CAM_IF_SUB_COM_NOTIFY_EVENT_SENSOR_I2C_ERROR (0x00000700)
#define E_CAM_IF_SUB_COM_NOTIFY_EVENT_SENSOR_MIPI_DATA_ERROR (0x00000800)
#define E_CAM_IF_SUB_COM_NOTIFY_EVENT_GYRO_ERROR (0x00000A00)

typedef struct Error_struct
{
	int image_capture_error[ERROR_CODE_COUNT];
	int video_recording_error[ERROR_CODE_COUNT];
	int sensor_i2c_error[COUNT_CAM];
	int sensor_MIPI_data_error[COUNT_CAM];
	//int sensor_eeprom_error[COUNT_CAM][ERROR_CODE_COUNT]; //new SNI update, not use for this firmware
	int gyro_error[ERROR_CODE_COUNT-3];
}Error_struct;

//----------------------------------------------------

static volatile E_CAM_IF_COM_SET camera_current_cmd_set;
static volatile E_CAM_IF_SUB_COM camera_current_cmd;

static volatile UCHAR camera_cmd_complete_flag = 0;

void cmd_record(inParams& inp, outParams& outp);
void cmd_recordtest(inParams& inp, outParams& outp);
int send_linux_cmd(string const& scmd, string& sresult);
void cmd_imu(inParams& inp, outParams& outp);



//-------Camera IPCU channel-----------
int camera_ipcu_init();
void camera_ipcu_close();
int ipcu_record();
//int open_device(ULONG device_number);
//int close_device(ULONG device_number);
//-------------------------------------


//------Notify IPCU channel------------
int notify_ipcu_init();
void notify_ipcu_close();
void cmd_notify_callback(UINT8 id, UINT32 pdata, UINT32 length, UINT32 cont, UINT32 total_length);
void decoding_notify();
//-------------------------------------


//-----------Gyroscope------------------------
int read_gyro_file(string const& filepatch);
void debugread_gyro_information();

int open_device(ULONG device_number);
int close_device(ULONG device_number);

int write_registr_imu(uint8_t reg, uint8_t data);
int read_registr_imu(uint8_t reg, uint8_t& dat);

//--------------------------------------------

#endif  //ORBI_TEST_WORKING_PART_OF_CAMERA_H