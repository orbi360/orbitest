#include "working_part_of_cm0.h"
#include <math.h>



// IPCU processing **************************************************************

static void cortex_cmd_rcv_callback(UINT8 id, UINT32 pdata, UINT32 length, UINT32 cont, UINT32 total_length)
{
    INT32 i;
    INT32 num;
    struct structCortexCmd ack;

    // if (g_exit_flag == 1) return;

    if (40 < length)
    {
        g_cortex_complete_flag = 1;
        //printf("Receive response error.\n");
        return;
    }

    num = length / sizeof(UINT32);
    memcpy(&ack.data[0], (void *)pdata, length);

    for (i = 0; i < num; ++i)
    {
        g_cortex_ret.data[i] = ack.data[i];
    }

    if (g_cortex_ret.data[0] >= D_PMIC_NOTIFY_CODE_OFFSET)
    {
        //printf("Receive notify.\n");
    }
    else if (g_cortex_ret.data[0] >= D_PMIC_RESPONSE_CODE_OFFSET)
    {
        //printf("Receive response.\n");
        g_cortex_complete_flag = 1;
    }
    else
    {
        //printf("Receive Unknown.\n");
        g_cortex_complete_flag = 1;
    }

// TEST for testlog
    // for (i = 0; i < num; ++i)
    // {
    //     printf("        Return %2d = 0x%08X\n", i, g_cortex_ret.data[i]); //here callback cortex
    // }

    return;
}


int cortex_ipcu_init()
{
    INT32 ret;

	/* Initialize IPCU */
	ret = FJ_IPCU_Open(E_FJ_IPCU_MAILBOX_TYPE_13, (UINT8*)&g_cortex_send_id);
	if (ret != FJ_ERR_OK)
		goto send_err;
	ret = FJ_IPCU_Open(E_FJ_IPCU_MAILBOX_TYPE_12, (UINT8*)&g_cortex_rcv_id);
	if (ret != FJ_ERR_OK)
		goto recv_err;
	ret = FJ_IPCU_SetReceiverCB(g_cortex_rcv_id, cortex_cmd_rcv_callback);
	if (ret != FJ_ERR_OK)
		goto cam_if_cb_err;
	return 0;

cam_if_cb_err:
	FJ_IPCU_Close(g_cortex_rcv_id);
recv_err:
	FJ_IPCU_Close(g_cortex_send_id);
	g_cortex_rcv_id = 0xFF;
send_err:
	g_cortex_send_id = 0xFF;
	return -1;
}

void cortex_ipcu_close()
{
    if (g_cortex_send_id != 0xFF)
		FJ_IPCU_Close(g_cortex_send_id);
	if (g_cortex_rcv_id != 0xFF)
		FJ_IPCU_Close(g_cortex_rcv_id);

	g_cortex_send_id = 0xFF;
	g_cortex_rcv_id = 0xFF;
}


static int cortex_send_cmd(ULONG param[LENGTH_CORTEX_CMD], INT32 num)
{
    INT32 i;
    INT32 ret;
    struct structCortexCmd cmd_req;
    UINT32 try_cnt = 0;
    UCHAR bTry_error = 0;
    UINT32 vaddr;

    vaddr = FJ_MM_phys_to_virt(D_TEST_LINUX_RTOS_COMMON_STRING_BUF);
    if (vaddr == (UINT32)MAP_FAILED)
    {
        printf("FJ_MM_phys_to_virt error\n");
        return -3;
    }

    g_cortex_complete_flag = 0;

    for (i = 0; i < num; ++i)
    {
        cmd_req.data[i] = param[i];
    }

    ret = FJ_IPCU_Send(g_cortex_send_id, (UINT32)&cmd_req, num * sizeof(ULONG), IPCU_SEND_SYNC);
    if (ret != FJ_ERR_OK)
    {
        FJ_IPCU_Close(g_cortex_send_id);
        return -1;
    }

    while (1)
    {
        if (g_cortex_complete_flag == 1)
        {
            break;
        }

		if (try_cnt > 10*100) {
			bTry_error = 1;
			break;
		}
		++try_cnt;
		//sleep(1);
		usleep(10000);
    }

    if (bTry_error == 1)
        return -2;
    return 0;
}


// CALENDER processing **********************************************

int ipcu_set_calendar_data(UINT32 second, UINT32 minute, UINT32 hour, UINT32 day,
                      UINT32 day_of_week, UINT32 month, UINT32 year)
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    INT32 num = 9;
    command[0] = 0x05;
    command[1] = 0x3001;
    command[2] = (ULONG)second;
    command[3] = (ULONG)minute;
    command[4] = (ULONG)hour;
    command[5] = (ULONG)day;
    command[6] = (ULONG)day_of_week;
    command[7] = (ULONG)month;
    command[8] = (ULONG)year;

    int err = cortex_send_cmd(command, num);
    return err;
}

int ipcu_get_calendar_data()
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    INT32 num = 2;
    command[0] = 0x06;
    command[1] = 0x3001;
    int err = cortex_send_cmd(command, num);
    return err;
}


int ipcu_start_temp_data()
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    //start temperature sensor
    INT32 num = 2;
    command[0] = 0x07;
    command[1] = 0x0A;
    int err = cortex_send_cmd(command, num);
    return err;
}

int ipcu_get_temp_data()
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    //get temp data
    INT32 num = 2;
    command[0] = 0x06;
    command[1] = 0x0A;
    int err = cortex_send_cmd(command, num);
    return err;
}

int ipcu_softoff()
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    INT32 num = 3;
    command[0] = 0x0A;
    command[1] = 0x02;
    command[2] = 0x0;
    int err = cortex_send_cmd(command, num);

    if(err) return err;

    num = 1;
    command[0] = 0x1;
    err = cortex_send_cmd(command, num);

    return err;
}

// I2C processing ********************************************

int ipcu_i2c_send(ULONG adr_slave, ULONG adr_reg, ULONG data)
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    INT32 num;

    //Only send
    command[0] = 0x02;
    command[1] = 0x01;
    command[2] = I2C_START_ADDRESS;
    command[3] = (adr_reg & 0xff) + ((data & 0xff) << 8);
    num = 4;

    int err = cortex_send_cmd(command, num);
    if(err) return err;

    command[0] = 5;
    command[1] = 7;
    command[2] = I2C_START_ADDRESS;
    command[3] = 2;
    command[4] = adr_slave;
    num = 5;
    err = cortex_send_cmd(command, num);
    if(err) return err;

    command[0] = 7;
    command[1] = 7;
    num = 2;
    err = cortex_send_cmd(command, num);
    if(err) return err;

    command[0] = 8;
    command[1] = 8;
    num = 2;
    if (cortex_send_cmd(command, num) != 0)
    err = cortex_send_cmd(command, num);
    return err;
}

int ipcu_i2c_read(ULONG adr_slave, ULONG adr_reg)
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    INT32 num;

    //Send+Receive
    command[0] = 0x02;
    command[1] = 2;
    command[2] = I2C_START_ADDRESS;
    command[3] = adr_reg;
    num = 4;
    int err = cortex_send_cmd(command, num);
    if(err) return err;

    command[0] = 5;
    command[1] = 7;
    command[2] = I2C_START_ADDRESS;
    command[3] = 1;
    command[4] = adr_slave;
    num = 5;
    err = cortex_send_cmd(command, num);
    if(err) return err;

    command[0] = 7;
    command[1] = 7;
    num = 2;
    err = cortex_send_cmd(command, num);
    if(err) return err;

    //Receive
    command[0] = 5;
    command[1] = 8;
    command[2] = I2C_BUFF_ADDRESS;
    command[3] = 1;
    command[4] = 0;
    command[5] = 0;
    command[6] = adr_slave;
    num = 7;
    err = cortex_send_cmd(command, num);
    if(err) return err;

    command[0] = 7;
    command[1] = 8;
    num = 2;
    err = cortex_send_cmd(command, num);
    if(err) return err;

    command[0] = 0x08;
    command[1] = 0x08;
    num = 2;
    err = cortex_send_cmd(command, num);
    if(err) return err;

    command[0] = 0x03;
    command[1] = 0;
    command[2] = I2C_BUFF_ADDRESS;
    num = 3;
    err = cortex_send_cmd(command, num);
    return err;
}

// CMD_from_iser ***********************************************************************

void cmd_calender(inParams& inp, outParams& outp)
{
    int err = 0;
    char ss[200];

    err = ipcu_get_calendar_data();
     // g_cortex_ret    <-- here result

    if(!err){
        if (inp.fTestlog){
           for(int i=0; i<LENGTH_CORTEX_CMD; i++)
              cout << (to_string((g_cortex_ret.data[i]) &0xffffffff) + "  ");
           cout <<endl;
        } 
        outp.ierr = 0;
//        outp.sout = "calender= [ss mm hh day dow month year] ";
//        for(int i=ID_CORTEX_DATA; i<LENGTH_CORTEX_CMD; i++)
//           outp.sout += (to_string((g_cortex_ret.data[i]) &0xffff) + "  ");
        sprintf(ss, "20%02d-%02d-%02d   %02d:%02d:%02d", g_cortex_ret.data[9], g_cortex_ret.data[8], g_cortex_ret.data[6], 
                            g_cortex_ret.data[5], g_cortex_ret.data[4],  g_cortex_ret.data[3]);
        outp.sout = string(ss);                    
    }else{
        outp.ierr = 1;
        outp.sout = "FAIL";
    }


    if(inp.fSelftestGet){
        double vsec1 = 0, vsec2 = 0;
        vsec1 = 60.*60.*g_cortex_ret.data[5] + 60.* g_cortex_ret.data[4] + g_cortex_ret.data[3];

        if(!err){
            sleep(5);
            err = ipcu_get_calendar_data();
           if(!err){
            if (inp.fTestlog){
                for(int i=0; i<LENGTH_CORTEX_CMD; i++)
                    cout << (to_string((g_cortex_ret.data[i]) &0xffffffff) + "  ");
                cout <<endl;
            } 
                outp.ierr = 0;
           }else{
                outp.ierr = 1;
                outp.sout = "FAIL";
           }
        }
       
      if(!outp.ierr){
        vsec2 = 60.*60.*g_cortex_ret.data[5] + 60.* g_cortex_ret.data[4] + g_cortex_ret.data[3];
        if(vsec2-vsec1 >= 4){
            outp.sout = "OK";
        }else{
            outp.ierr = 1;
            outp.sout = "FAIL";
        }    
      }
    }

}

void cmd_i2c(inParams& inp, outParams& outp)
{
    int err = 0;
    char ss[200];

try{

  // if pmic-mode or pmic-volt - then complex output
  if(inp.fPMICmode || inp.fPMICvoltage || inp.fPMICtest){

    if(inp.fPMICtest){
        if(inp.fSetGet){
            uint32_t ui2 = 0;
            uint32_t ui3 = 0;
            uint8_t iret = 0;

            switch(inp.ui2){
            case 0:  
	// 	  ipt.ui2 = 0x12;  // dcdc5 - NRM_volt
	// 		ipt.ui3 = 0x11;  // 1.2+0.15V, default=0x10
                ui2 = 0x12;
                ui3 = inp.ui3 &0x1f;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23005);

                sleep(1);

	// 	  ipt.ui2 = 0x19;  // dcdc5 - SLP_volt
	// 		ipt.ui3 = 0x11;  // 1.2+0.15V, default=0x10
                ui2 = 0x19;
                ui3 = inp.ui3 &0x1f;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23005);

                sleep(1);

	// 	  ipt.ui2 = 0x0c;  // config - dc5[nrm-slp]+dc6[nrm-slp]
	// 		ipt.ui3 = 0x22;  // not sleep for 5, defaultDVT=0x32
                ui2 = 0x0c;
                ui3 = 0x22;//inp.ui3 &0x1f;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23005);

                sleep(1);

	// 	  ipt.ui2 = 0x12;  // dcdc236 - NRM_volt
	// 		ipt.ui3 = 0x11;  // 0.9-0.012V, default=0x10
                ui2 = 0x10;
                ui3 = inp.ui2 &0x1F;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
                ui2 = 0x11;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
                // ui2 = 0x13;
                // if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);

                sleep(1);

                ui3 = inp.ui3 &0x1f;
                ui2 = inp.ui2 &0x1f;

               break;

            default:
               throw(23007);       
            }

            sprintf(ss,"i2c-set-pmic-test: ch5 [nrm,slp,no_sleep] =0x%02x   ch23[nrm] =0x%02x", ui3, ui2);
            outp.sout = string(ss);
            outp.ierr = 0;

        }else{
            uint8_t iret = 0;
            uint32_t ui2 = 0;
            int v1, v2, v3;

            ui2 = 0x0C;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23007);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             v1 = iret &0xff;
            ui2 = 0x12;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23007);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             v2 = iret &0x1f;
            ui2 = 0x19;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23007);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             v3 = iret &0x1f;

            sprintf(ss,"i2c-get-pmic-test[dc5: mode-vnrm-vslp]:HEX %02x-%02x-%02x", v1, v2 ,v3);
            outp.sout = string(ss);
            outp.ierr = 0;
        }    

    }// if pmic-test

    if(inp.fPMICmode){
        if(inp.fSetGet){
            uint32_t ui2 = 0;
            uint32_t ui3 = 0;
            uint8_t iret = 0;

            switch(inp.ui2){
            case 5:  
                ui2 = 0x0C;

                if(ipcu_i2c_read(inp.ui1, ui2)) throw(23002);
                 iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;

                ui3 = ((inp.ui3 &0x0f)<<4) &0xf0;
                ui3 = ui3 | (iret &0x0f);

                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23004);
               break;

            case 56:  
                ui2 = 0x0C;
                ui3 = inp.ui3 &0xff;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23004);
               break;

            case 123:  
                ui2 = 0x0E;
                ui3 = inp.ui3 &0xff;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23004);
               break;

            default:
               throw(23005);       
            }

            sprintf(ss,"i2c-set-pmic-mode: %d(0x%02x) =0x%02x", inp.ui2, ui2 ,ui3);
            outp.sout = string(ss);
            outp.ierr = 0;

        }else{
            uint8_t iret = 0;
            uint32_t ui2 = 0;
            int m1, m2, m3, m4, m5, m6, m7, ml2, ml3;

            ui2 = 0x0A;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23002);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             m1 = (iret >> 4)&0x0f;
             m2 = iret &0x0f;
            ui2 = 0x0B;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23002);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             m3 = (iret >> 4)&0x0f;
             m4 = iret &0x0f;
            ui2 = 0x0C;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23002);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             m5 = (iret >> 4)&0x0f;
             m6 = iret &0x0f;
            ui2 = 0x0D;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23002);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             m7 = (iret >> 4)&0x0f;
//             m2 = iret &0x0f;
            ui2 = 0x0E;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23002);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             ml2 = (iret >> 4)&0x0f;
             ml3 = iret &0x0f;

            sprintf(ss,"i2c-get-pmic-mode[dc1-dc7 ldo2-ldo3]:HEX %01x-%01x..%01x-%01x..%01x-%01x..%01x   %01x-%01x", m1, m2 ,m3, m4, m5, m6, m7, ml2, ml3);
            outp.sout = string(ss);
            outp.ierr = 0;
        }    

    }// if pmic-mode

    if(inp.fPMICvoltage){
        if(inp.fSetGet){
            uint32_t ui2 = 0;
            uint32_t ui3 = 0;

            switch(inp.ui2){
            case 1:  
                ui2 = 0x0F;
                ui3 = inp.ui3 &0x0F;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
               break;
            case 2:  
                ui2 = 0x10;
                ui3 = inp.ui3 &0x1F;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
               break;
            case 3:  
                ui2 = 0x11;
                ui3 = inp.ui3 &0x1F;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
               break;
            case 5:  
                ui2 = 0x12;
                ui3 = inp.ui3 &0x1F;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
               break;
            case 6:  
                ui2 = 0x13;
                ui3 = inp.ui3 &0x1F;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
               break;
            case 7:  
                ui2 = 0x14;
                ui3 = inp.ui3 &0x0F;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
               break;
            case 236:  
                ui2 = 0x10;
                ui3 = inp.ui3 &0x1F;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
                ui2 = 0x11;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);
                ui2 = 0x13;
                if(ipcu_i2c_send(inp.ui1, ui2, ui3)) throw(23012);

                ui2 = 0xff;
               break;
            default:
               throw(23011);       
            }

            sprintf(ss,"i2c-set-pmic-volt: %d(0x%02x) =0x%02x", inp.ui2, ui2 ,ui3);
            outp.sout = string(ss);
            outp.ierr = 0;

        }else{

            uint8_t iret = 0;
            uint32_t ui2 = 0;
            int xv1, xv2, xv3, xv5, xv6, xv7;
//            int xvs1, xvs2, xvs3, xvs5, xvs6, xvs7;

            ui2 = 0x0F;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23013);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             xv1 = iret &0xff;
            ui2 = 0x10;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23013);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             xv2 = iret &0xff;
            ui2 = 0x11;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23013);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             xv3 = iret &0xff;
            ui2 = 0x12;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23013);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             xv5 = iret &0xff;
            ui2 = 0x13;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23013);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             xv6 = iret &0xff;
            ui2 = 0x14;    
            if(ipcu_i2c_read(inp.ui1, ui2)) throw(23013);
             iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
             xv7 = iret &0xff;

            sprintf(ss,"i2c-get-pmic-volt[dc1-dc7]:HEX %02x-%02x-%02x .. %02x-%02x-%02x", xv1, xv2 ,xv3, xv5, xv6, xv7);
            outp.sout = string(ss);
            outp.ierr = 0;

        }

    }// if pmic-voltage

  }else{  // normal I2C *****************************************************************

    if(inp.fSetGet){
        err = ipcu_i2c_send(inp.ui1, inp.ui2, inp.ui3);

        if(!err){
            outp.ierr = 0;
            sprintf(ss,"i2c-set: 0x%02x  0x%02x << 0x%02x", inp.ui1, inp.ui2, inp.ui3);
            outp.sout = string(ss);
        }

    }else{
        err = ipcu_i2c_read(inp.ui1, inp.ui2);

        if(!err){
            outp.ierr = 0;
              uint8_t iret = 0;
              iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
            sprintf(ss,"i2c-get: 0x%02x  0x%02x = 0x%02x", inp.ui1, inp.ui2, iret);
            outp.sout = string(ss);
        }
    }// if set-get
  }

}catch(...){
    err = 1;
}

    if(!err){
        if (inp.fTestlog){
           for(int i=0; i<LENGTH_CORTEX_CMD; i++)
              cout << (to_string((g_cortex_ret.data[i]) &0xffff) + "  ");
           cout <<endl;
        } 
    }else{
        outp.ierr = 1;
        outp.sout = "FAIL";
    }

}

void cmd_temperature(inParams& inp, outParams& outp)
{
    int err = 0;
    char ss[200];
    double vTsoc=0, vTadc=0, vTcharger=0;

  try{

    if(!fTempStarted){
        err = ipcu_start_temp_data();
        if (inp.fTestlog) cout << "  ..start of temperature_sensor_m10v" <<endl;
        if(inp.fTemperatureAll){
// TEST !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1            
             err = cortex_adc_start();
             if (inp.fTestlog) cout << "  ..start of temperature_sensor_adc" <<endl;
        }    
        fTempStarted = 1;
        sleep(1);
    }    
    if(err) throw(10);

    // M10V temperature ***
    err = ipcu_get_temp_data();  // g_cortex_ret    <-- here result
    if(!err){
        if (inp.fTestlog){
           cout << "m10v_iT:   "; 
           for(int i=0; i<LENGTH_CORTEX_CMD; i++)
              cout << (to_string((g_cortex_ret.data[i]) &0xffffff) + "  ");
           cout <<endl;
        } 

        vTsoc = ((g_cortex_ret.data[ID_CORTEX_DATA]) &0xffff)/8.;
    }else throw(20);

    if(inp.fTemperatureAll){
        // charger temperature ***
        err = ipcu_i2c_read(I2C_ADDR_CHARGER, 10);
        if(!err){
            vTcharger = 1.*(g_cortex_ret.data[ID_CORTEX_DATA] &0xff);
            if(inp.fTestlog){ cout << "charger_vT:  " <<vTcharger <<endl;};

        }else throw(30);

        // adc temperature ***
        err = cortex_get_adc_value();
        if(!err){
            ULONG iTtmp = (g_cortex_ret.data[ID_CORTEX_DATA] &0x03ff);
            if(inp.fTestlog){ cout << "adc_iT:  " <<iTtmp <<endl;};

            if((iTtmp>100)&&(iTtmp<1000)){
                vTadc = 1./(1./(273+25.) +log(1024./iTtmp -1)/3455.) -273.;
            }else{
                vTadc = 100.;
            }
//            vTadc = 100.*iTtmp/1024.;

        }else throw(30);

    }


//        outp.sout = "temp= " +to_string((int)vTemp) + " C";
        if(inp.fTemperatureAll){
            sprintf(ss, "%.0f   %.0f   %.0f", vTsoc, vTcharger, vTadc);
            if(inp.fTestlog) sprintf(ss, "Tsoc=%.0f   Tcharger=%.0f   Tadc=%.0f", vTsoc, vTcharger, vTadc);
        }else{    
            sprintf(ss, "%d", (int)vTsoc);
        }
        
        outp.ierr = 0;
        outp.sout = string(ss);

  }catch(...){
        if(inp.fTestlog) cout << "  error in try_catch.." <<endl;
        err = 1;
  }  

    if(err){
        outp.ierr = 1;
        outp.sout = "FAIL";
    }    

}


int start_charger(int fLog)
{
    int err = 0;
    char ss[200];

    ULONG areg = 0;
    uint8_t iret = 0;

//   if(inp.fTestlog){ 
//     // control reg-s
//     areg = 0;
//     err = ipcu_i2c_read(I2C_ADDR_CHARGER, areg);
//     if(!err){
//         iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
//         sprintf(ss,"i2c-get: 0x%02x  0x%02x = 0x%02x", I2C_ADDR_CHARGER, areg, iret);
//         cout << ss <<endl;
//     }
//     areg = 1;
//     err = ipcu_i2c_read(I2C_ADDR_CHARGER, areg);
//     if(!err){
//         iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
//         sprintf(ss,"i2c-get: 0x%02x  0x%02x = 0x%02x", I2C_ADDR_CHARGER, areg, iret);
//         cout << ss <<endl;
//     }
//   }  
// if(inp.fTestlog){cout << err <<"  " <<iret<<endl;};

    // RUN bit (0x10=1) + MIXED_MODE bit(0x01=0) to the control reg
    areg = 0;
    err = ipcu_i2c_send(I2C_ADDR_CHARGER, areg, 0x18);
    if(!err){
        iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
        if(fLog){
          sprintf(ss,"i2c-set: 0x%02x  0x%02x = 0x%02x", I2C_ADDR_CHARGER, areg, 0x18);
          cout << ss <<endl;
        }
    }

   return err;
}

void cmd_charger(inParams& inp, outParams& outp)
{
    int err = 0;
    char ss[200];

    ULONG areg = 0;
    uint8_t iret = 0;

    uint8_t iT = 0;
    uint16_t iU = 0;
    uint16_t iI = 0;

    // temperature
//     areg = 10; 
//     err = ipcu_i2c_read(I2C_ADDR_CHARGER, areg);
//     if(!err){
//         iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
//         sprintf(ss,"charger: T= %d", iret);
//         cout << ss <<endl;
//         itemp = iret;
//     }
// if(inp.fTestlog){cout << err <<"  " <<iret<<endl;};

// TEST !!!!!!!!!!!!!!!!!!!!!!!!!!
 // fChargerStarted = 1; 

   if(!fChargerStarted){
      err = start_charger(inp.fTestlog);
      fChargerStarted = 1;
      if(inp.fTestlog) cout << "  start of charger..  ";
      sleep(1);
   }


    // voltage
    areg = 8; 
    err = ipcu_i2c_read(I2C_ADDR_CHARGER, areg);
    if(!err){
        iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
        sprintf(ss,"charger: V8= %d", iret);
        if(inp.fTestlog) cout << ss <<endl;
        iU = iret;
    }
    

    areg = 9; 
    err = ipcu_i2c_read(I2C_ADDR_CHARGER, areg);
    if(!err){
        iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
        sprintf(ss,"charger: V9= %d", iret);
        if(inp.fTestlog) cout << ss <<endl;
        iU += (iret <<8);
    }
if(inp.fTestlog){cout << err <<"  " <<iret<<endl;};

    // current
    areg = 6; 
    err = ipcu_i2c_read(I2C_ADDR_CHARGER, areg);
    if(!err){
        iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
        sprintf(ss,"charger: I6= %d", iret);
        if(inp.fTestlog) cout << ss <<endl;
        iI = iret;
    }
    areg = 7; 
    err = ipcu_i2c_read(I2C_ADDR_CHARGER, areg);
    if(!err){
        iret = g_cortex_ret.data[ID_CORTEX_DATA] &0xff;
        sprintf(ss,"charger: I7= %d", iret);
        if(inp.fTestlog) cout << ss <<endl;
        iI += (iret <<8);
    }
if(inp.fTestlog){cout << err <<"  " <<iret<<endl;};

    double vT, vU, vI;
    vT = iT;
    vU = iU*0.0022; // Volt
    int fCharge = 1;

    // convert sign
    if(iI &0x8000){
      iI = (~iI)+0x01;
      fCharge = 0;
    }

    vI = iI*0.00588/0.010; // mA

    double vOCV = vU + vI*0.001*0.28;

    outp.ierr = 0;
 //   sprintf(ss, "T=%d   V=%.2f   I=%.0f", (int)vT, vU, vI);
    if(inp.fShortPrint){
        sprintf(ss, "%.2f   %.3f      %.2f", vU, ((fCharge)?vI/1000.*-1.:vI/1000.), vU*vI/1000.);
    }else{
        sprintf(ss, "V=%.2f V   I=%.0f mA %s     OCV(280R)=%.2f V", vU, vI, ((fCharge)?"(charging)":" "), vOCV);
    } 
    outp.sout = string(ss);

}

int cortex_adc_start()
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    INT32 num = 4;

    command[0] = 0x02;
    command[1] = 0x01;
    command[2] = ADC_BUFFER_ADDRESS;
    command[3] = 0x00;

    int err = cortex_send_cmd(command, num);
    if(err) return err;

    num = 3;

    command[0] = 0x03;
    command[1] = 0x01;
    command[2] = ADC_BUFFER_ADDRESS;

    if((std::uint16_t)g_cortex_ret.data[ID_CORTEX_DATA] != 0x00)
    {
        return err;
    }

    err = cortex_send_cmd(command, num);
    if(err) return err;

    num = 4;

    command[0] = 0x05;
    command[1] = 0x09;
    command[2] = ADC_BUFFER_ADDRESS;
    command[3] = 0x02; // 2 byte data

    err = cortex_send_cmd(command, num);
    if(err) return err;

    num = 3;

    command[0] = 0x05;
    command[1] = 0x1009;
    command[2] = 0x00;

    err = cortex_send_cmd(command, num);
    if(err) return err;

    num = 2;

    command[0] = 0x07;
    command[1] = 0x09;

    err = cortex_send_cmd(command, num);
    return err;
}

int cortex_get_adc_value()
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    INT32 num = 3;

    command[0] = 0x03;
    command[1] = 0x01;
    command[2] = ADC_BUFFER_ADDRESS;

    int err = cortex_send_cmd(command, num);
    return err;
}

int cortex_adc_stop()
{
    ULONG command[LENGTH_CORTEX_CMD] = {0};
    INT32 num = 2;

    command[0] = 0x08;
    command[1] = 0x09;

    int err = cortex_send_cmd(command, num);
    return err;
}
