#ifndef ORBI_TEST_WORKING_PART_OF_CM0_H
#define ORBI_TEST_WORKING_PART_OF_CM0_H

#include "headers_and_constants.h"
#include "inparser.h"


//-----Cortex-----

#define D_TEST_LINUX_RTOS_COMMON_STRING_BUF		(0x4FE40028)

#define D_PMIC_RESPONSE_CODE_OFFSET	0x01000000
#define D_PMIC_NOTIFY_CODE_OFFSET	0x02000000

static volatile UINT8 g_cortex_send_id = 0xFF;
static volatile UINT8 g_cortex_rcv_id = 0xFF;

static volatile UCHAR g_cortex_complete_flag = 0;

//----------------

static volatile unsigned long I2C_START_ADDRESS = 0x6FE62000;
static volatile unsigned long I2C_BUFF_ADDRESS = 0x6FE62010;

static const unsigned long ADC_BUFFER_ADDRESS = 0x6FE62020;

//----------------
enum ID_CORTEX_RETURNs{ID_CORTEX_CMD=0, ID_CORTEX_SUBCMD, ID_CORTEX_ERR, ID_CORTEX_DATA};

#define LENGTH_CORTEX_CMD   (10)

struct structCortexCmd {
	ULONG data[LENGTH_CORTEX_CMD];
};
static volatile struct structCortexCmd g_cortex_ret;

//----------------------------


int cortex_ipcu_init();
void cortex_ipcu_close();

int ipcu_set_calendar_data (UINT32 second, UINT32 minute, UINT32 hour, UINT32 day,
                        UINT32 day_of_week, UINT32 month, UINT32 year);

int ipcu_get_calendar_data();
int ipcu_get_temp_data();
int ipcu_start_temp_data();
int ipcu_softoff();
int ipcu_i2c_send(ULONG adr_slave, ULONG adr_reg, ULONG data);
int ipcu_i2c_read(ULONG adr_slave, ULONG adr_reg);


int calendar(UINT32 second, UINT32 minute, UINT32 hour, UINT32 day,
                        UINT32 day_of_week, UINT32 week, UINT32 month, UINT32 year);


int start_charger(int);

void cmd_calender(inParams& inp, outParams& outp);
void cmd_temperature(inParams& inp, outParams& outp);
void cmd_i2c(inParams& inp, outParams& outp);
void cmd_charger(inParams& inp, outParams& outp);

int cortex_adc_start();
int cortex_get_adc_value();
int cortex_adc_stop();

static int fTempStarted = 0;
static int fChargerStarted = 0;

#endif  //ORBI_TEST_WORKING_PART_OF_CM0_H