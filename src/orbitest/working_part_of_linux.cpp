#include "working_part_of_linux.h"
#include "working_part_of_cm0.h"
#include <boost/format.hpp>



 extern int fSignalStop;
// int fButtonsListenStop;
// int idButtonListen;

int send_linux_cmd(string const& scmd, string& sresult)
{
    int err = 0;
    char buffer[255];
    sresult = "";
    FILE* pipe = NULL;

    try{
        pipe = popen(scmd.c_str(), "r");

        while (!feof(pipe)) {
            if (fgets(buffer, 254, pipe) != NULL)
                sresult += buffer;
        }

        pclose(pipe);
        err = 0;
    }
    catch (...){
       if(pipe) pclose(pipe); 
       err = 1; 
    }
    
    return err;
}


void cmd_init(inParams &inp, outParams &outp)
{
    outp.ierr = 1;

    ofstream osfile;

if(inp.iTypeIO==TYPEIO_PWM){
    //PWM for LEDs
    try
    {
        if (inp.fTestlog) cout << "  init(): set fs.pwm  " <<endl;
        for (int iled = 0; iled < N_LEDS; iled++)
        {
            string spath = SYSFS_PWM_DIR + LED_CHIPs[iled] + "/export";
            osfile.open(spath.c_str());
            osfile << LED_PWMs[iled];
            osfile.close();
            spath = SYSFS_PWM_DIR + LED_CHIPs[iled] + "/pwm" + LED_PWMs[iled] + "/period";
            osfile.open(spath.c_str());
            osfile << LED_PWM_PERIOD;
            osfile.close();
            spath = SYSFS_PWM_DIR + LED_CHIPs[iled] + "/pwm" + LED_PWMs[iled] + "/duty_cycle";
            osfile.open(spath.c_str());
            osfile << "0";
            osfile.close();
            spath = SYSFS_PWM_DIR + LED_CHIPs[iled] + "/pwm" + LED_PWMs[iled] + "/enable";
            osfile.open(spath.c_str());
            osfile << "0";
            osfile.close();
        }

        //      outp.ierr = 0;
    }
    catch (...)
    {
        if (osfile.is_open())
            osfile.close();
        if (inp.fTestlog)
            cout << "  error: init().pwm  ";
        outp.ierr = 1;
        outp.sout = "FAIL";
        return;
    } // try pwm
}

if(inp.iTypeIO==TYPEIO_EVT){
    //EVT-type: PWM & GPIO-RR for LEDs
    try
    {
        if (inp.fTestlog) cout << "  init(): set fs.pwm  " <<endl;
        for (int iled = 0; iled < N_LEDS; iled++)
        {
          if(iled!=LED_ID_RR){  
            string spath = SYSFS_PWM_DIR + LED_CHIPs[iled] + "/export";
            osfile.open(spath.c_str());
            osfile << LED_PWMs[iled];
            osfile.close();
            spath = SYSFS_PWM_DIR + LED_CHIPs[iled] + "/pwm" + LED_PWMs[iled] + "/period";
            osfile.open(spath.c_str());
            osfile << LED_PWM_PERIOD;
            osfile.close();
            spath = SYSFS_PWM_DIR + LED_CHIPs[iled] + "/pwm" + LED_PWMs[iled] + "/duty_cycle";
            osfile.open(spath.c_str());
            osfile << "0";
            osfile.close();
            spath = SYSFS_PWM_DIR + LED_CHIPs[iled] + "/pwm" + LED_PWMs[iled] + "/enable";
            osfile.open(spath.c_str());
            osfile << "0";
            osfile.close();
          }// if iled != RR  
        }// for iled

        //      outp.ierr = 0;
    }
    catch (...)
    {
        if (osfile.is_open())
            osfile.close();
        if (inp.fTestlog)
            cout << "  error: init().pwm  ";
        outp.ierr = 1;
        outp.sout = "FAIL";
        return;
    } // try pwm
}
    //GPIO for LEDs
    // it is already init-ed in Linux

    //GPIO for BUTTNOS
    try
    {
        if (inp.fTestlog) cout << "  init(): set fs.gpio  " <<endl;
        for (int ibtn = 0; ibtn < N_BUTTONS; ibtn++)
        {
            string spath = SYSFS_GPIO_DIR + "/export";
            osfile.open(spath.c_str());
            osfile << BUTTON_GPIOs[ibtn];
            osfile.close();
            spath = SYSFS_GPIO_DIR + "gpio" + BUTTON_GPIOs[ibtn] + "/direction";
            osfile.open(spath.c_str());
            osfile << "in";
            osfile.close();
            spath = SYSFS_GPIO_DIR + "gpio" + BUTTON_GPIOs[ibtn] + "/active_low";
            osfile.open(spath.c_str());
            osfile << BUTTON_ACTIVE_LOWs[ibtn];
            osfile.close();
        }
    }
    catch (...)
    {
        if (osfile.is_open())
            osfile.close();
        if (inp.fTestlog)
            cout << "  error: init().gpio  ";
        outp.ierr = 1;
        outp.sout = "FAIL";
        return;
    } // try gpio


    //GPIO for POWER_OFF
    try
    {
        if (inp.fTestlog) cout << "  init(): set fs.gpio.poweroff  " <<endl;
            string spath = SYSFS_GPIO_DIR + "/export";
            osfile.open(spath.c_str());
            osfile << GPIO_POWER_OFF;
            osfile.close();
            spath = SYSFS_GPIO_DIR + "gpio" + GPIO_POWER_OFF + "/direction";
            osfile.open(spath.c_str());
            osfile << "out";
            osfile.close();
            spath = SYSFS_GPIO_DIR + "gpio" + GPIO_POWER_OFF + "/active_low";
            osfile.open(spath.c_str());
            osfile << "0";
            osfile.close();
    }
    catch (...)
    {
        if (osfile.is_open())
            osfile.close();
        if (inp.fTestlog)
            cout << "  error: init().gpio.poweroff  ";
        outp.ierr = 1;
        outp.sout = "FAIL";
        return;
    } // try gpio

    //IPCU init
    // if(ipcu_init() != 0){
    //     if(inp.fTestlog)  cout << "  error: init().ipcu  ";
    //     outp.ierr = 1;
    //     outp.sout = "FAIL";
    // }

   // start TEMPERATURE measurements
   int err = ipcu_start_temp_data();
   if (inp.fTestlog) cout << "  start of temperature..";

   // start CHARGER-VOLTAGE measurements
   err = start_charger(inp.fTestlog);
   if (inp.fTestlog) cout << "  start of charger..";
 
   // start ADC measurements
   err = cortex_adc_start();
   if (inp.fTestlog) cout << "  ..start of temperature_sensor_adc" <<endl;

    outp.ierr = 0;
    outp.sout = "OK";
    return;
}

void cmd_version(inParams &inp, outParams &outp)
{
    ifstream ifs_vers;
    string s_version = "VERSION";
    try
    {
        string spath = ORBI_VERSION_FILE;
        if (inp.fTestlog) cout << "  read file: " << spath <<endl;
        
        ifs_vers.open(spath.c_str());

        string s_line;
        while(getline(ifs_vers, s_line))
        {
            size_t ipos = s_line.find_last_not_of(" \t\n\r");
            s_version += "  " + s_line.substr(0,ipos+1);
        }

        s_version += "\n";

        ifs_vers.close();
    }
    catch (...)
    {
        if (ifs_vers.is_open())
            ifs_vers.close();
        outp.ierr = 1;
        outp.sout = "FAIL";
        return;
    } // try gpio266

   outp.ierr = 0;
   outp.sout = s_version;
 //   outp.sout = ("test build: " __DATE__ "  " __TIME__);
}

void cmd_poweroff(inParams &inp, outParams &outp)
{
   if(inp.fSoftOff){
      
      int err = ipcu_softoff();
      if(err){
        outp.ierr = 1;
        outp.sout = "FAIL";
        return;
      }

   }else{
        
    ofstream osfile;
    try
    {
        string spath = SYSFS_GPIO_DIR +"gpio" +GPIO_POWER_OFF + "/value";
        if (inp.fTestlog) cout << "  set 1 to: " << spath <<endl;
            osfile.open(spath.c_str());
            osfile << "1";
            osfile.close();
    }
    catch (...)
    {
        if (osfile.is_open())
            osfile.close();
        outp.ierr = 1;
        outp.sout = "FAIL";
        return;
    } // try gpio266

   }// if-else SOFTOFF

    outp.ierr = 0;
    outp.sout = "OK";
}

void cmd_led(inParams &inp, outParams &outp)
{
    // calc duty_cicles
    int iDutys[N_LEDS] = {0};
    double scale = 1. * LED_PWM_PERIOD / 255.;
    iDutys[0] = (inp.ui1 & 0xff) * scale;
    iDutys[1] = (inp.ui2 & 0xff) * scale;
    iDutys[2] = (inp.ui3 & 0xff) * scale;
    iDutys[3] = (inp.ui4 & 0xff) * scale;
    for (int i = 0; i < N_LEDS; i++)
    {
        if (iDutys[i] < 1)
            iDutys[i] = 0;
    }

    // set duties and enables for PWM
    ofstream osfile;
    try
    {
        if (inp.fTestlog) cout << "  led():  " <<endl;
        for (int iled = 0; iled < N_LEDS; iled++)
        {
          int fLedPWM = 0;
          if(inp.iTypeIO==TYPEIO_PWM) fLedPWM = 1;
          if((inp.iTypeIO==TYPEIO_EVT) && (iled!=LED_ID_RR)) fLedPWM=1;  

          if(fLedPWM){  
            if (inp.fTestlog) cout << "  led[" << iled << "]=" << iDutys[iled];
            string spath = SYSFS_PWM_DIR + (LED_CHIPs)[iled] + "/pwm" + (LED_PWMs)[iled] + "/duty_cycle";
            osfile.open(spath.c_str());
            osfile << iDutys[iled];
            osfile.close();
            spath = SYSFS_PWM_DIR + (LED_CHIPs)[iled] + "/pwm" + (LED_PWMs)[iled] + "/enable";
            osfile.open(spath.c_str());
            osfile << ((iDutys[iled] > 0) ? "1" : "0");
            osfile.close();
          }else{
            string spath = SYSFS_LEDGPIO_DIR + "led_" + (LED_GPIOs)[iled] + "/brightness";
            osfile.open(spath.c_str());
            osfile << ((iDutys[iled] > 0) ? "1" : "0");
            osfile.close();
          }// if-else !GPIO  
        }
        if (inp.fTestlog) cout <<endl;
    }
    catch (...)
    {
        if (osfile.is_open())
            osfile.close();
        if (inp.fTestlog)
            cout << "  error: led().pwm  ";
        outp.ierr = 1;
        outp.sout = "FAIL";
        return;
    } // try pwm

    outp.ierr = 0;
    outp.sout = "OK";
}
/*
void thread_buttons_listen()
{
    int cnt_ms = 0;
    int cnt_press = 0;
    int cnt_no_press = 0;
    const int WAIT_CNT_BUTTON = 2;


    while((!fButtonsListenStop) && (!fSignalStop) && (cnt_ms<MAX_BUTTONS_LISTEN_S*1000)){

       this_thread::sleep_for(chrono::milliseconds(BUTTON_LOOP_PERIOD_MS));
       cnt_ms+=BUTTON_LOOP_PERIOD_MS;

       int ierr = 0;
       int btn_pressed = -1;

       ifstream isfile;
       try{

           for (int ibtn=0; ibtn<N_BUTTONS; ibtn++)
           {
               string spath = SYSFS_GPIO_DIR +"gpio" +BUTTON_GPIOs[ibtn] + "/value";
                 isfile.open(spath.c_str(),ios_base::in);
                 int ivalue;
                 isfile >> ivalue;
                 isfile.close();
                 if(ivalue>0) btn_pressed = ibtn;
           }

       }catch(...){
           if(isfile.is_open()) isfile.close();
           ierr = 1;
           fButtonsListenStop = 1;
       }// try gpio

       if(ierr){
         cout << "button: error" <<endl;
       }else{

         if((btn_pressed>=0)){

           // take button-press into account only after several empty-ticks    
           if(cnt_no_press>WAIT_CNT_BUTTON){

              //if ALL then to show only 
              if(idButtonListen==N_BUTTONS){
                cout << "button: " <<BUTTON_NAMEs[btn_pressed] <<endl;

              }else{

                //if pressed the button which is waited for  
                if(btn_pressed == idButtonListen){
                    //if pressed several ticks
                    if(cnt_press>=WAIT_CNT_BUTTON){
                       fButtonsListenStop = 1;
                       cout << "button: " <<BUTTON_NAMEs[btn_pressed] <<endl; 
                    }
                    cnt_press++;

                }else{
                    cnt_press=0;
                }

              }// if-else wait-ALL 
           }// if cnt_press>N 


         }else{
            cnt_no_press++;
            cnt_press=0;
         }// if-else pressed

       }// if-else ierr

    }// while fBtnListenStop

}

void cmd_button(inParams &inp, outParams &outp)
{
   idButtonListen = inp.ui1;

    if (inp.fTestlog) cout << "  cmd_button(): " <<idButtonListen <<endl;
    fButtonsListenStop = 0;
    thread th_btn_listen(thread_buttons_listen);
    th_btn_listen.detach();

    if (idButtonListen == N_BUTTONS)
    {
        if (inp.fTestlog) cout << "for test: sleep(" <<MAX_BUTTONS_LISTEN_S <<"s)" <<endl;
        sleep(MAX_BUTTONS_LISTEN_S);
    }else{
// TEST
        if (inp.fTestlog) cout << "for test: sleep(" <<MAX_BUTTONS_LISTEN_S <<"s)" <<endl;
        sleep(MAX_BUTTONS_LISTEN_S);
    }
//    else{
//        if (inp.fTestlog) cout << "  buttons_listen()): off  " <<endl;
//        fButtonsListenStop = 1;
//        this_thread::sleep_for(chrono::milliseconds(BUTTON_LOOP_PERIOD_MS + 10));
//    }

    outp.ierr = 0;
    outp.sout = "OK";
}
*/

void cmd_button(inParams &inp, outParams &outp)
{
    int fButtonsListenStop = 0;
    int idButtonListen = inp.ui1;
    int iMaxButtonListen_ms = inp.ui2*1000;//MAX_BUTTONS_LISTEN_S*1000;

    int cnt_ms = 0;
    int cnt_press = 0;
    int cnt_no_press = 0;
    const int WAIT_CNT_BUTTON = 2;

    int fFound = 0;

    while((!fButtonsListenStop) && (!fSignalStop) && (cnt_ms<iMaxButtonListen_ms)){

       this_thread::sleep_for(chrono::milliseconds(BUTTON_LOOP_PERIOD_MS));
       cnt_ms+=BUTTON_LOOP_PERIOD_MS;
       
       int btn_pressed = -1;

       ifstream isfile;
       try{

           for (int ibtn=0; ibtn<N_BUTTONS; ibtn++)
           {
               string spath = SYSFS_GPIO_DIR +"gpio" +BUTTON_GPIOs[ibtn] + "/value";
                 isfile.open(spath.c_str(),ios_base::in);
                 int ivalue;
                 isfile >> ivalue;
                 isfile.close();
                 if(ivalue>0) btn_pressed = ibtn;
           }

       }catch(...){
           if(isfile.is_open()) isfile.close();
           fButtonsListenStop = 1;
       }// try gpio

       if(!fButtonsListenStop){

         if((btn_pressed>=0)){

           // take button-press into account only after several empty-ticks    
           if(cnt_no_press>WAIT_CNT_BUTTON){

              //if ALL then to show only 
              if(idButtonListen==N_BUTTONS){
                cout << "button: " <<BUTTON_NAMEs[btn_pressed] <<endl;

              }else{

                //if pressed the button which is waited for  
                if(btn_pressed == idButtonListen){
                    //if pressed several ticks
                    if(cnt_press>=WAIT_CNT_BUTTON){
                       fButtonsListenStop = 1;
                       //cout << "button: " <<BUTTON_NAMEs[btn_pressed] <<endl; 
                       fFound = 1;
                    }
                    cnt_press++;

                }else{
                    cnt_press=0;
                    // more strict condition: if false button >> FAIL
                    fButtonsListenStop=1;
                }// if-else pressed which is waited for

              }// if-else wait-ALL 
           }// if cnt_press>N 


         }else{
            cnt_no_press++;
            cnt_press=0;
         }// if-else pressed

       }// if !err==!fButtonListenStop

    }// while fBtnListenStop

    // temporary for WAIT_ALL
    if(idButtonListen==N_BUTTONS) fFound = 1;

    if(fFound){
      outp.ierr = 0;
      outp.sout = "OK";
    }else{
      outp.ierr = 1;
      outp.sout = "FAIL";
    }  
    
}
void cmd_wifi(inParams &inp, outParams &outp)
{
    outp.ierr = 1;
    outp.sout = "FAIL";

    string s_temp = "";
    int err = send_linux_cmd("ifconfig -a | grep \"uap0\"", s_temp);
    if(err){
        if(inp.fTestlog) cout << "cmd_wifi(): error in ifconfig.." <<endl;
        return;
    }     
        //if(inp.fTestlog) cout << "MAC_ADDR: " <<s_temp <<endl;

    size_t ipos = s_temp.find_last_not_of(" \t\n\r");
    string s_mac_addr = "ORBI_";
    if((ipos>=12) && (s_temp.length() > 12)){     // the string should be long at least =  XX:XX:XX:XX:XX:XX
       ipos -= 7;                                // three last bytes XX:XX:XX
      s_mac_addr += s_temp.substr(ipos,2);
       ipos += 3; 
      s_mac_addr += s_temp.substr(ipos,2);
       ipos += 3; 
      s_mac_addr += s_temp.substr(ipos,2);
    }else{
        s_mac_addr += "xxxxxx";
    }

    if(inp.fTestlog) cout << "new_SSID: " <<s_mac_addr <<endl;

        //if(inp.fTestlog) cout << "open file: " <<ORBI_HOSTAPD_CONF_TMPL_FILE <<endl;

    ifstream istr_tmpl(ORBI_HOSTAPD_CONF_TMPL_FILE, std::ios::binary);
    if(!istr_tmpl.good()){
        if(inp.fTestlog) cout << "cmd_wifi(): error in hostapd_conf_tmpl.." <<endl;
        return;
    }

    string ss_hostapd_tmpl = string((istreambuf_iterator<char>(istr_tmpl)), istreambuf_iterator<char>());
    string ssfile = (boost::format(ss_hostapd_tmpl) % s_mac_addr % 48 % "orbiglaz" % "a" % 1).str();

    ofstream ostr_conf("/etc/hostapd.conf", std::ios::binary | std::ios::trunc);
    if(!ostr_conf.good()){
        if(inp.fTestlog) cout << "cmd_wifi(): error in hostapd.conf.." <<endl;
        return;
    }     

    ostr_conf << ssfile;
    ostr_conf.flush();
    ostr_conf.close();
    system("/usr/bin/orbi/start-wifi-ap.sh >> /dev/null");
        //if(inp.fTestlog) cout << "cmd_wifi(): /usr/bin/orbi/start-wifi-ap.sh" <<endl;





    ifstream ifs_cfg("/run/NF1/ORBI.CFG", std::ios::in);
    if(!ifs_cfg.good()){
        if(inp.fTestlog) cout << "cmd_wifi(): error in orbicfg.." <<endl;
        return;
    }

    ofstream ofs_cfg1("/run/NF1/ORBI1.CFG");

    string s_line;
    while(getline(ifs_cfg, s_line))
    {
        size_t pos = s_line.find("\"ssid\"");
        if (pos != string::npos)     s_line = "\"ssid\" : \"" + s_mac_addr + "\",";
        pos = s_line.find("\"psk\"");
        if (pos != string::npos)     s_line = "\"psk\" : \"orbiglaz\",";

        ofs_cfg1 << s_line << '\n';
    }

    ifs_cfg.close();
    ofs_cfg1.close();

    system("rm -f /run/NF1/ORBI.CFG");
    system("mv -f /run/NF1/ORBI1.CFG /run/NF1/ORBI.CFG");



    outp.ierr = 0;
    outp.sout = s_mac_addr;//"OK";
}
