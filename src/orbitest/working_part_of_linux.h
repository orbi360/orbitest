#ifndef ORBI_TEST_WORKING_PART_OF_LINUX_H
#define ORBI_TEST_WORKING_PART_OF_LINUX_H

#include "headers_and_constants.h"
#include "inparser.h"
#include <thread>
#include <boost/format.hpp>
#include "working_part_of_camera.h"


#define SYSFS_GPIO_DIR (string)"/sys/class/gpio/"
#define SYSFS_PWM_DIR  (string)"/sys/class/pwm/"
#define SYSFS_LEDGPIO_DIR  (string)"/sys/devices/platform/gpio-leds/leds/"

#define ORBI_HOSTAPD_CONF_TMPL_FILE "/usr/bin/orbi/HOSTAPD.TPL"
#define ORBI_VERSION_FILE   "/etc/version.txt"

#define CAMERA_IF_NAME  "camera_if10"
#define CMD_CAMERA_MODE1    CAMERA_IF_NAME" chg_camera_mode 1 | grep -r Return"  
#define CMD_CAMERA_MODE2    CAMERA_IF_NAME" chg_camera_mode 2 | grep -r Return"  
#define CMD_CAMERA_MODE3    CAMERA_IF_NAME" chg_camera_mode 3 | grep -r Return"  
#define CMD_CAMERA_MODE8    CAMERA_IF_NAME" chg_camera_mode 8 | grep -r Return"  
#define CMD_CAMERA_CFG1   CAMERA_IF_NAME" s_v_frame_size 32 5 1 0 | grep -r Return"  
#define CMD_CAMERA_CFG2   CAMERA_IF_NAME" s_v_bitrate 0 20000000 20000000 0 | grep -r Return"  
#define CMD_CAMERA_CFG2low   CAMERA_IF_NAME" s_v_bitrate 0 15000000 15000000 0 | grep -r Return"  
#define CMD_CAMERA_CFG3   CAMERA_IF_NAME" s_a_audio_enable 1 | grep -r Return"  
#define CMD_CAMERA_CFG4   CAMERA_IF_NAME" s_a_mic_sellection 1 | grep -r Return"  



enum LED_IDs{LED_ID_RR=0, LED_ID_B, LED_ID_G, LED_ID_R, N_LEDS};
#define LED_CHIPs    ((string []){"pwmchip0", "pwmchip0", "pwmchip4", "pwmchip4"})
#define LED_PWMs     ((string []){"0",     "3",     "2",     "3"})
#define LED_PWM_PERIOD      (2000000)
#define LED_MAX_VALUE       (255)
#define LED_GPIOs     ((string []){"2",     "1",     "3",     "4"})


enum BUTTON_IDs{btnSHUTTER0=0, btnMODE, btnWIFI, btnPOWER, N_BUTTONS};
#define BUTTON_GPIOs ((string []){"265", "270", "271", "259"})
#define BUTTON_ACTIVE_LOWs ((string []){"1", "1", "1", "0"})
#define BUTTON_NAMEs ((string []){"shutter", "mode", "wifi", "power"})
#define BUTTON_LOOP_PERIOD_MS    (100)
//#define MAX_BUTTONS_LISTEN_S    (20)

#define GPIO_POWER_OFF  (string)"266"


int send_linux_cmd(string const& scmd, string& sresult);

void cmd_init(inParams& inp, outParams& outp);
void cmd_version(inParams& inp, outParams& outp);
void cmd_led(inParams& inp, outParams& outp);
void cmd_button(inParams& inp, outParams& outp);
void cmd_wifi(inParams& inp, outParams& outp);
void cmd_poweroff(inParams& inp, outParams& outp);

#endif  //ORBI_TEST_WORKING_PART_OF_LINUX_H
