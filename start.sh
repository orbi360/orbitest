#!/bin/sh

if [[ $1 != "again" ]]
then

  echo "kill connman" > /dev/ttyUSI0
  kill -9 `ps | grep [c]onnmand | awk '{print $1}'` &
  wait 

  echo "mount SD" > /dev/ttyUSI0

  mkdir -p /run/RTOS
  mount -t ipcufs mnt /run/RTOS/ -ofs=8
  chmod 777 /run/RTOS

  echo "mount NF" > /dev/ttyUSI0

  mkdir -p /run/NF1
  mount -t ipcufs mnt /run/NF1 -ofs=1
  chmod 777 /run/NF1

  if [[ -e "/run/RTOS/CLEAR.MD" ]]
  then
    echo "clear NAND(start scrip)" > /dev/ttyUSI0
    rm -f /run/NF1/START.SH
  fi

  if [[ -e "/run/RTOS/START.SH" ]]
  then
    echo "copy new start script" > /dev/ttyUSI0

    cp -f /run/RTOS/START.SH /run/NF1/START.SH
    chmod 755 /run/NF1/START.SH
  fi

  if [[ -e "/run/NF1/START.SH" ]]
  then
    echo "run NF start script" > /dev/ttyUSI0

    chmod 755 /run/NF1/START.SH
    /run/NF1/START.SH again &

    exit 0
  fi
fi


if [[ -e "/run/RTOS/CLEAR.MD" ]]
then

  echo "clear NAND" > /dev/ttyUSI0  
  
  rm -f /run/NF1/ORBICTL
  rm -f /run/NF1/STARTW.SH
  rm -f /run/NF1/STOPW.SH
  rm -f /run/NF1/UDHCPC.SH
  rm -f /run/NF1/HOSTAPD.TPL
  rm -f /run/NF1/WPA.CFG
  rm -f /run/NF1/ORBI.CFG
  rm -f /run/NF1/HOSTAPD.TPL
  rm -f /run/NF1/T2I.TPL
  rm -f /run/NF1/T2V.TPL
fi

echo "copy new OrbiCtl and configs" > /dev/ttyUSI0

cp -f /run/RTOS/ORBICTL /run/NF1/ORBICTL
cp -f /run/RTOS/STARTW.SH /run/NF1/STARTW.SH
cp -f /run/RTOS/STOPW.SH /run/NF1/STOPW.SH
cp -f /run/RTOS/UDHCPC.SH /run/NF1/UDHCPC.SH
cp -f /run/RTOS/HOSTAPD.TPL /run/NF1/HOSTAPD.TPL
cp -f /run/RTOS/WPA.CFG /run/NF1/WPA.CFG
cp -f /run/RTOS/ORBI/ORBI.CFG /run/NF1/ORBI.CFG
cp -f /run/RTOS/ORBI/HOSTAPD.TPL /run/NF1/HOSTAPD.TPL
cp -f /run/RTOS/ORBI/T2I.TPL /run/NF1/T2I.TPL
cp -f /run/RTOS/ORBI/T2V.TPL /run/NF1/T2V.TPL


cp -f /run/NF1/ORBICTL /usr/bin/orbi/orbictl
cp -f /run/NF1/STARTW.SH "/usr/bin/orbi/start-wifi-ap.sh"
cp -f /run/NF1/STOPW.SH "/usr/bin/orbi/stop-wifi-ap.sh"
cp -f /run/NF1/UDHCPC.SH /usr/bin/orbi/udhcpc.sh
#cp -f /run/NF1/HOSTAPD.TPL /usr/bin/orbi/hostapd.conf.tmpl
cp -f /run/NF1/WPA.CFG /etc/wpa_supplicant.conf

chmod 755 /usr/bin/orbi/orbictl
chmod 755 "/usr/bin/orbi/start-wifi-ap.sh"
chmod 755 "/usr/bin/orbi/stop-wifi-ap.sh"
chmod 755 /usr/bin/orbi/udhcpc.sh
chmod 755 /etc/wpa_supplicant.conf


kill $(pidof camera_Ev_rec)

echo "mount debugfs" > /dev/ttyUSI0

mount -t debugfs none /sys/kernel/debug
cat /sys/kernel/debug/gpio > /dev/ttyUSI0


echo "release soft block" > /dev/ttyUSI0

busybox rfkill unblock 0
busybox rfkill unblock 0 &
wait
busybox rfkill list all &
wait

echo "attach WiFi driver" > /dev/ttyUSI0

export COUNTRY=US
insmod /usr/bin/orbi/azre/bin_sd8xxx/mlan.ko &
wait
insmod /usr/bin/orbi/azre/bin_sd8xxx/sd8xxx.ko drv_mode=3 fw_name=mrvl/sd8887_uapsta_a2.bin cal_data_cfg=none &
wait

mv /dev/random /dev/random.orig
ln -s /dev/urandom /dev/random


#if [[ -e "/run/RTOS/WIFIMODE.ST" ]]
#then

#  echo "start WIFI stantion mode" > /dev/ttyUSI0

#  ip link set mlan0 up &
#  wait
#  if [ "$(ifconfig | grep mlan0)" ]
#  then
#    ifconfig mlan0 0.0.0.0
#    wpa_supplicant -B -D wext -i mlan0 -c /etc/wpa_supplicant.conf

#    sleep 4s
#    busybox udhcpc -i mlan0 -s /usr/bin/orbi/udhcpc.sh -b
#  fi

#else

#  echo "start AP mode" > /dev/ttyUSI0

#  iw dev uap0 interface add uap0 type __ap &
#  wait
#  ifconfig uap0 192.168.2.1 netmask 255.255.255.0
#  /usr/bin/orbi/azre/hostapd/hostapd -B -P /var/run/hostapd.pid /etc/hostapd.conf &
#  wait
#  echo "start DHCP" > /dev/ttyUSI0
#  busybox udhcpd -f /etc/udhcpd.conf &

#fi

echo "attach MTP driver" > /dev/ttyUSI0

insmod /lib/modules/4.4.15/kernel/drivers/usb/gadget/udc/sn_usb20_udc.ko &
wait
sh /home/root/usb_test/usb30dev_mtp_func.sh
#sh /home/root/usb_test/mtp_func.sh

echo "start SSHD" > /dev/ttyUSI0

mkdir /var/run/sshd
/usr/sbin/sshd

echo "start NFS" > /dev/ttyUSI0

mkdir /run/orbi
/usr/sbin/rpcbind
/usr/bin/unfsd -s


#echo "start OrbiCtl" > /dev/ttyUSI0

#mkdir /run/RTOS/ORBI
if [[ -e "/run/RTOS/ORBITEST" ]]

then

echo "orbictl is NOT started" > /dev/ttyUSI0
echo "camifts &" > /dev/ttyUSI0
camifts &

else
 
/usr/bin/orbi/orbictl /run/NF1/ORBI.CFG &

fi

echo "--!! TEST TEST TEST boot!!---" > /dev/ttyUSI0
